#include <stdio.h>

main() {

    int c;
    while((c = getchar()) != EOF) {
	if(c == '\t') {
	    putchar("    ");
	    putchar(c);
	    c = getchar();
	}
    }
    return 0;
}
