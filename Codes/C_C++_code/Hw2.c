#include <stdio.h>

extern int numodd( int array[], int size ) ;

int main( int argc, char * argv[] )
{
	int numarray[] = { 2, 3, 1025, 3024, 4057, -3, -1025, -3578 } ;
	int size = sizeof(numarray) / sizeof( int ) ;
	int result ;
	
	result = numodd( numarray, size ) ;
	printf( "Number of odd numbers: %d\n", result ) ;
}
