#include <stdio.h>

main() 
{
	int c;
	
	c = getchar();
	
	// checking value of EOF
	printf("%d\n",EOF);

	while(c != EOF)
	{
		putchar(c);
		c = getchar();
	}
}


/*
This is more concise
	while((c=getchar() != EOF))
		putchar(c);
*/
