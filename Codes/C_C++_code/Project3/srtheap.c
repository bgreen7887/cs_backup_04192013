/*
Bilal Green 
Project 3

*/


#include <stdio.h>
#include <stdlib.h>
#include "srt.h"


void srtheap(void* base, size_t nelem, size_t size, int (*compar)(const void *, const void *)){

	char *a, *b;
	size_t i, j, k;
	if (nelem <= 1) return;
	
	for (k = nelem / 2+ 1; --k; 1) {
		for (i = k; nelem >= (j = i * 2); i = j) {
			a = (char *)base + (j - 1) * size;  
				if (j < nelem && compar(a, a + size) <= 0) {
					a += size;
					++j;
					}
					b = (char *)base + (i - 1) * size;
				if (compar(a, b) < 0) {
					break;
			}
			swap(b, a, size);
		}
	}
	
	while (nelem > 1) {
		a = (char *)base;
		b = (char *)base + (nelem - 1) * size;
		swap(a, b, size);
		--nelem;
		for (i = 1; nelem >= (j = i * 2); i = j) {
			a = (char *)base + (j - 1) * size;
			if (j < nelem && compar(a, a + size) < 0) {
				a += size;
				++j;
			}
			b = (char *)base + (i - 1) * size;
			if (compar(a, b) <= 0) { // <=
				break;
			}
			swap(b, a, size);
		}
	}
	return;
}
