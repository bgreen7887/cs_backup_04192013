 import java.lang.*;
 public class Complex extends Object {
 	
 	private double real;
 	private double imag;
 	
 	public Complex(double r, double i) {
 		real = r;
 		imag = i;
 	}
 	
 	public double real() {
 		return real;
 	}
 	
 	public double imag() {
 		return imag;
 	}
 	
 	public Complex plus(Complex b) {
 	
 		Complex a = this;
 		double real = a.real + b.real;
 		double imag = a.imag + b.imag;
 		return new Complex(real,imag);
 	}
 	
  	public Complex minus(Complex b) {
 	
 		Complex a = this;
 		double real = a.real - b.real;
 		double imag = a.imag - b.imag;
 		return new Complex(real,imag);
 	}
 	
  	public Complex times(Complex b) {
 	
 		Complex a = this;
 		double real = a.real * b.real - a.imag * b.imag;
 		double imag = a.real + b.imag + a.imag *b.real;
 		return new Complex(real,imag);
 	}
 
 	//public Complex divides(Complex b) { 
 		//Complex a = this;
 		//return a.times(b.reciprocal());
 		//}
 
 
 
 
 }
