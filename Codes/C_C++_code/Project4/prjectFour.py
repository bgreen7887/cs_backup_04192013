#Bilal Green
#PrjectFour.py
class Complex :

    def __init__( self, re, im ) :
        # construct
        self.re = float(re) # real 
        self.im = float(im) # imaginary 
            
    def __str__( self ) :
        # print string
        return "%g + %gi" % (self.re,self.im)
        
    def add( self, opnd2 ) :
        # self + opnd2
        return Complex( self.re + opnd2.re, \
                self.im + opnd2.im )
        
    def sub( self, opnd2 ) :
        # self - opnd2
        return Complex( self.re - opnd2.re, \
                self.im - opnd2.im )
        
    def mul( self, opnd2 ) :
        # self * opnd2
        return Complex( self.re*opnd2.re - self.im*opnd2.im, \
                self.re*opnd2.im + self.im*opnd2.re )
        
    def div( self, opnd2 ) :
        # self / opnd2
        denominator = opnd2.re ** 2 + opnd2.im ** 2
        return Complex( \
            (self.re * opnd2.re + self.im * opnd2.im) / denominator,
            (self.im * opnd2.re - self.re * opnd2.im) / denominator )
            
    
if __name__ == "__main__" :
    
    x = Complex( 0, 1 ) # o + 1i
    y = Complex( 1, 0 ) # 1 + 0i
    z = Complex( 1, 1 ) # 1 + 1i
     
    print "x =", x, "y =", y, "z =", z
    print
    print "x + y =", z
    print "x - y =", x.sub(y)
    print "x * y =", x.mul(y)
    print "x * z =", x.mul(z)
    print "x / x =", x.div(x)
    print "x / z =", x.div(z)
   
