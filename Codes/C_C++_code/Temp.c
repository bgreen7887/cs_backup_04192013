/*
 * Displays Fahrenheit to Celsius table
 *
 */

#include<stdio.h>

main() {
	float fahr,celsius;
	int lower, upper, step;

	lower = 300;
	upper = 0;
	step = 20;

	fahr = lower;
	
	printf("%s\t%s\t%s","FAHREN.","\t----->","CELSIUS\n");
	printf("%s\t\t\t%s","_______","______\n\n");
	while (fahr > upper) {
		celsius = (5.0/9.0) * (fahr-32.0) ;
 		printf("%3.0f\t\t\t%6.1f\n",fahr, celsius);
		fahr = fahr - step;
	}
}
