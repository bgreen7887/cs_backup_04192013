/*
 * Count # of tabs, spaces, and newlines
 *
 */
 
 #include <stdio.h>
 
 main() {
 	int c,nt,ns,nl;
 		c = nt = ns = nl = 0;
 	
 	while ((c = getchar()) != EOF) 
 		if ( c == '\n') 
 			++nl;
 		else if ( c == '\t')
 			++nt;
 		else if ( c == ' ')
 			++ns;
 	printf("Number of tabs: %d\nNumber of spaces: %d\nNumber of newlines: %d\n",nt,ns,nl); 
 
 return 0;
 }
