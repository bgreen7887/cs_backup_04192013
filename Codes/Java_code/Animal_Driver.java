public class Animal_Driver
 {
	public static void main(String[] args) 
	{
		Animal a = new Animal();
		System.out.println(a.str);
		Dog d = new Dog();
		System.out.println(d.str);
		Poodle p = new Poodle();
		System.out.println(p.str);
		System.out.println("");
		
		System.out.println(d.str);
		d =(Dog) p; // poodle can become a dog
		System.out.println(d.str);
		System.out.println("");
		
		System.out.println(a.str);
		a =(Animal) d; // Animal becoming a dog
		System.out.println(a.str);
		System.out.println("");
		
		
		System.out.println(p.str);
		p =(Poodle) d; // Dog cannot be cast to poodle
		System.out.println(p.str);
		
		System.out.println(a.str);
		d =(Dog) a; // Animal cannot be cast to dog
		System.out.println(a.str);
	}
}