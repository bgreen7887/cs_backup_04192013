public class Automobile 
{
	protected String bodyType,transType;
	protected int maxSpeed;
	
	public Automobile(String bodyType,String transType,int maxSpeed) 
		{
			this.bodyType = bodyType;
			this.transType = transType;
			this.maxSpeed = maxSpeed;
		}
		
			public void setBodyType(String bT)
				{
					bodyType = bT;
				}
		
			public String getBodyType()
					{
					return bodyType;
					} 
			
			public void setTransType(String tT)
				{
					transType = tT;
				}
		
			public String getTransType()
					{
					return transType;
					} 
			
			public void setMaxSpeed(int mS)
				{
					maxSpeed = mS;
				}
		
			public int getMaxSpeed()
					{
					return maxSpeed;
					} 
			




}