 import java.text.DecimalFormat;
 public class BaseBallPlayer {
 	
 		private String name;
 		private double adverage;
 		
 	public BaseBallPlayer(String name, double adverage) {  
 		this.name = name;
 		this.adverage = adverage;
 	}
 	
 	public String getName() {
 		return name;
 	}
 	
 	public double getAdverage() {
 		return adverage;
 	}
 	
 	public String getAdverageString() {
 		DecimalFormat decFormat = new DecimalFormat("     .000");
 		return decFormat.format(adverage);
	}
 }
