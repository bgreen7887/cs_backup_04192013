public class Book{
    //public int pages = 1500;
    protected int pages = 1500;
	 protected String publisher;
	 protected int year;
	 protected int isbn;
	 protected String author;
	 protected int volume;
	 
	 public Book(int pages,String publisher,int year,int isbn,String author,int volume)
	 {
			this.pages = pages;
			this.publisher = publisher;
			this.year = year;
			this.isbn = isbn;
			this.author = author;
			this.volume = volume;
		}

   	public void setPages (int numPages)  
	 	{
      		pages = numPages;
   	}

   		public int getPages ()  
			 {
				return pages;
			} 

  public void setPublisher (String publisher)   {
      this.publisher = publisher;
   }

   public String getPublisher ()   {
      return publisher;
} 

  public void setYear (int year)   {
      this.year = year;
   }

   public int getYear ()   {
      return year;
} 


  public void setIsbn (int isbn)   {
      this.isbn = isbn;
   }

   public int getIsbn ()   {
      return isbn;
} 

  public void setAuthor (String author)   {
      this.author = author;
   }

   public String getAuthor ()   {
      return author;
} 

public void setVolume (int volume)   {
      this.volume = volume;
   }

   public int getVolume ()   {
      return volume;
} 




 
}
