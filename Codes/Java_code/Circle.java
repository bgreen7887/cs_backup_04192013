public class Circle {
 
//// ATTRIBUTES ////

private int blue;
private int green;
private int red;
private Point origin;
private double radius;

//// CONSTRUCTOR ////

public Circle(Point org, double rad)
 {
    red = 0;
    green = 0;
    blue = 0;
    origin = org;
    radius = rad;  
 }

public int getBlue()
 {
    return blue;	
 }
public int getGreen()
 {
    return green;	
 }
public int getRed()
 {
    return red;	
 }

public Point getOrigin()
 {
    return origin;	
 }
public double getRadius()
 {
    return radius;	
 }
public void setOrigin(Point org)
 {
    origin.setPoint(org.getX(), org.getY());	
 }
public void setRadius(double r)
 {
    radius = r;	
 }
public void setRGB(int r, int b, int g)
 {
    red = r;
    blue = b;
    green = g;		
 }

}
