import java.lang.Math;

public class CircleShape extends Shape {

private double radius;

// constructors //

public CircleShape() {
    super();
    radius = 0.0;
}
public CircleShape(final Point org, final double rad) {
    super();
    radius = rad;
}
public double getRadius()
{
    return radius;
}
public void setRadius(double rad)
{
    radius = rad;
}
public double Area()
{
   return Math.PI * radius * radius;
}
public double Perimeter()
{
    return 2 *Math.PI * radius;
}  
}
