public class Color {
 
//// ATTRIBUTES ////

private int blue;
private int green;
private int red;


//// CONSTRUCTOR ////

public Color(int r, int b, int g)
 {
    red = r;
    green = g;
    blue = b;
 }

public int getBlue()
 {
    return blue;	
 }
public int getGreen()
 {
    return green;	
 }
public int getRed()
 {
    return red;	
 }
public void setRGB(int r, int b, int g)
 {
    red = r;
    blue = b;
    green = g;		
 }

}
