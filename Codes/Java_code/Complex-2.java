public class Complex
	{
	public double realCoefficient;
    public double imaginaryCoefficient;

    public Complex() 
		{
        this(0);
		}

    public Complex(double imaginaryCoefficient) 
		{
        this(0, imaginaryCoefficient);
		}

    public Complex(double realCoefficient, double imaginaryCoefficient) 
		{
        this.realCoefficient = realCoefficient;
        this.imaginaryCoefficient = imaginaryCoefficient;
		}

    public Complex add(Complex o) 
		{
        return new Complex((realCoefficient + o.realCoefficient), (imaginaryCoefficient + o.imaginaryCoefficient));
		}

    public Complex add(double n) 
		{
        return new Complex((realCoefficient), (imaginaryCoefficient + n));
		}

    public Complex div(Complex o) 
		{
		double a = realCoefficient; double b = imaginaryCoefficient;
		double c = o.realCoefficient; double d = o.imaginaryCoefficient;
        return new Complex(((a * c + b * d)/(Math.pow(c, 2) + Math.pow(d, 2))), ((b * c - a * d)/(Math.pow(c, 2) + Math.pow(d, 2))));
		}

    public Complex div(double n) 
		{
		double a = realCoefficient; double b = imaginaryCoefficient;
		double c = 0; double d = n;
        return new Complex(((a * c + b * d)/(Math.pow(c, 2) + Math.pow(d, 2))), ((b * c - a * d)/(Math.pow(c, 2) + Math.pow(d, 2))));
		}

    public Complex mul(Complex o) 
		{
		double a = realCoefficient; double b = imaginaryCoefficient;
		double c = o.realCoefficient; double d = o.imaginaryCoefficient;
        return new Complex((a * c - b * d), (b * c + a * d));
		}

    public Complex mul(double n) 
		{
		double a = realCoefficient; double b = imaginaryCoefficient;
		double c = 0; double d = n;
        return new Complex((a * c - b * d), (b * c + a * d));
		}

    public Complex sub(Complex o) 
		{
        return new Complex((realCoefficient - o.realCoefficient), (imaginaryCoefficient - o.imaginaryCoefficient));
		}

    public Complex sub(double n) 
		{
        return new Complex((realCoefficient), (imaginaryCoefficient - n));
		}

    public String toString() 
		{
        return "(" + realCoefficient + " + " + imaginaryCoefficient + "i)";
		}
	}
