public class Dictionary extends Book{
   private int definitions = 52500;
	protected int edition = 2;
	
	
	public Dictionary(int pages,String publisher,int year,int isbn,String author,int volume,int definitions,int edition)
	{
		super(pages, publisher,year,isbn, author, volume);
		this.definitions= definitions;
		this.edition = edition;
	}

   public double computeRatio ()   {
      return definitions/pages;
   }

   public void setDefinitions (int numDefinitions)   {
      definitions = numDefinitions;
   }

   public int getDefinitions ()   {
      return definitions;
}   }

