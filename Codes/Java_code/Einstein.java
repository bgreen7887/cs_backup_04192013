 /* 
 Einstein.java
 Bilal Green 
 1/10/2012
 */
 
 import javax.swing.JApplet;
 import java.awt.*;
 
 public class Einstein extends JApplet
 {
 
 	public void paint (Graphics page)
 
 		page.drawRect ( 50, 50, 40, 40 );
 		page.drawRect ( 60, 80, 220, 40 );
 		page.drawOval ( 50, 50, 40, 40 );
 		page.drawLine ( 20, 50, 120, 110 );
 		
 		page.drawString ("Out of clutter, find simplicity.", 110, 70 );
 		page.drawString ("-- Albert Einstein", 130, 100);
 	}
 }
