//A -> I = E
//E -> T + E | T - E | T
//T -> F * T | F / T | F
//F -> P ^ F | P
//P -> I | L | (E)
//I -> a | b | ... | y | z
//L -> 0 | 1 | ... | 8 | 9

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GrammarParse
	{
	public static int i;
	public static String finalString;
	
	public static void main(String[] args)
		{
		//Get string from file
		File in = new File("input.txt");
		Scanner scan = null;
		try
			{
			scan = new Scanner(in);
			}
		catch(FileNotFoundException e)
			{
			System.exit(0);
			}

		//string is the stirng to be tested
        while (scan.hasNext()) 
			{
            String string;
			string = scan.nextLine();
			finalString = string;
			
			if(A() && i == finalString.length())
				{
			System.out.println("The string " + finalString + " is in the language.");
				}
			else
				{
			System.out.println("The string " + finalString + " is not in the language.");
				}
			}
		}
		
		//Check if the string is an Assignment
		//A -> I = E
		public static boolean A()
			{
			if (I()) 
				{
				if (i < finalString.length() && finalString.charAt(i) == '=') 
					{
					++i;
					if (E()) 
						{
						return true;
						}
					else 
						{
						return false;
						}
					}
				return true;
				}
			return false;
			}
		
		//Check if the string is an Expression
		//E -> T + E | T - E | T
		public static boolean E()
			{
			if(T())
				{
				if(i < finalString.length() && (finalString.charAt(i) == '+' || finalString.charAt(i) == '-'))
					{
					++i;
					if(E())
						{
						return true;
						}
					else
						{
						return false;
						}
					}
				else
					{
					return true;
					}
				}
			return false;
			}
		
		//Check if the string is a Term
		//T -> F * T | F / T | F
		public static boolean T() 
			{	
			if (F()) 
				{
				if (i < finalString.length() && (finalString.charAt(i) == '*' || finalString.charAt(i) == '/')) 
					{
					++i;
					if (T()) 
						{
						return true;
						}
					else 
						{
						return false;
						}
					}
				else
					{
					return true;
					}
				}
			return false;
			}
			
		//Check if the string is a Factor
		// P ^ F | P
		public static boolean F()
			{
			if(P())
				{
				if(i < finalString.length() && finalString.charAt(i) == '^')
					{
					++i;
					if(F())
						{
						return true;
						}
					else
						{
						return false;
						}
					}
				else
					{
					return true;
					}
				}
			return false;
			}
		
		//Check if the string is a Power	
		// P -> I | L | (E)
		public static boolean P()
			{
			if(I())
				{
				return true;
				}
			else if(L())
				{
				return true;
				}
			else if(i < finalString.length() && finalString.charAt(i) == '(')
				{
				++i;
				if(E())
					{
					if(i < finalString.length() && finalString.charAt(i) == ')')
						{
						++i;
						return true;
						}
					else
						{
						return false;
						}
					}
				else
					{
					return false;
					}
				}
			return false;
			}

		//Check if the string is an Identifier
		// I -> a | b | ... | y | z
		public static boolean I()
			{
			if(i < finalString.length() && (finalString.charAt(i) >= 'a' && finalString.charAt(i) <= 'z'))
				{
				++i;
				return true;
				}
			return false;
			}
		
		//Check if the string is a Literal
		//L -> 0 | 1 | ... | 8 | 9
		public static boolean L()
			{
			if(i < finalString.length() && (finalString.charAt(i) >= '0' && finalString.charAt(i) <= '9'))
				{
				++i;
				return true;
				}
			return false;
			}
	}
