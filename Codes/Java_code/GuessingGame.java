 import static java.lang.System.out;
 import java.util.Scanner;
 import java.util.Random;
 
 public class GuessingGame {
 
 	public static void main (String[] args) {
 		Scanner myScanner = new Scanner(System.in);
 		
 		System.out.print("Enter an int from 1 to 10: ");
 		
 		int inputNumber = myScanner.nextInt();
 		int randomNumber = new Random().nextInt(10) + 1;
 		
 		if (inputNumber == randomNumber) {
 			System.out.println("***************");
 			System.out.println("**You Win.**");
 			System.out.println("***************");
 		} else {
 			System.out.print("You Lose 1s.");
 			System.out.print("The Random Number was ");
 			System.out.println(randomNumber + ".");
 		}
 
 	}
 }
