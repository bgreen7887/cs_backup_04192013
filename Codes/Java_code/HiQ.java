import java.util.Scanner;
public class HiQ {

	int [] puzzle; 
	int [] hole;
	String [] solutions;
	String move;
	String saveMove;
	String x ="yes";
	String y="no";
	int p = 0;
	int h = 0;
	int s = 0;
	Scanner scan = new Scanner(System.in);

	int start,finalPegs;

// ********************************* constructor!  // *********************************
	public HiQ(int s, int f){ 
	
	start = s;
	finalPegs = f;

	puzzle = new int[15];
	hole = new int[14];
	solutions = new String[13];

	for(int i = 0; i<puzzle.length; i++) {    // POPULATE PUZZLE ARRAY W/ ALL 1 FOR FILLED SPACE !
		puzzle[i] = 1;
	
		} // end 'for puzzle'
		
	puzzle[start] = 0;			   // CHANGE START SPOT FROM 1 -> 0, UPDATE HOLE ARRAY  !
	hole[0]=start;				  
	p--;
	h++;	

	} // end constructor
// ********************************* recursive funct!  // *********************************
		 void findSolutions() {
		if(start == finalPegs)
			String line = scan.nextLine();
			if(line.equals(x)) {

				finalPegs --;
				//System.out.print("You've picked yes");
				findSolutions();
				} // END INNER IF !

			else if(line.equals(y)) {
				System.out.print("You've picked no");
				// do nothing
		        }  // END ELSE IF !
		
		} // END IF !
		else{
			System.out.print("Invalid Entry");		
		} 
		else {
			int x =0;
		
	
 // ********************************* saving holes  // *********************************
	for(int i =0; i<puzzle.length;i++) {
		if(puzzle[i] == 0) {
	
		x =0;
		i = hole[x];
		x++;
	
		else {
			// do nothing 
		}
		} // END FOR
		}
		}
	for(int i = 0; i<hole.length; i++){
		
		int x = hole[i];
		move = Move(x);
		
		if(move == "Invalid Move"){
			i++;
		}

			else {

				solutions[s] = move;
				s++;
			}
				
			findSolutions();		

	} // END FOR !
	
 }// END RECURSIVE FUNCTION !

 // ********************************* move function  // *********************************

	public String Move(int p){
	
	int point = p;
	
					// IF 0 EMPTY // 
		if (point == 0){
			if (puzzle[3] == 1 && puzzle[1] == 1) {	
				puzzle[3] = 0; puzzle[1] = 0; puzzle[0]=1;
				p--;
				h++;
				saveMove = "(3,1,0)";
			
			} //END INNER IF !

			else if (puzzle[5] == 1 && puzzle[2] == 1){
				puzzle[5] = 0; puzzle[2] = 0; puzzle[0]=1;
				p--;
				h++;
				saveMove = "(5,2,0)";

			} //END ELSE IF

		} // END IF !

//*****************************************************************************************
			// IF 1 EMPTY // 

		if (point == 1){
			if (puzzle[6] == 1 && puzzle[3] == 1) {	
				puzzle[6] = 0; puzzle[3] = 0; puzzle[1]=1;
				p--;
				h++;
				saveMove = "(6,3,1)";
			
			} // END INNER IF !

			else if (puzzle[8] == 1 && puzzle[4] == 1){
				puzzle[8] = 0; puzzle[4] = 0; puzzle[1]=1;
				p--;
				h++;
				saveMove = "(8,4,1)";
 
			} // END ELSE IF

		} // END IF !

//*****************************************************************************************
			// IF 2 EMPTY // 
		if (point == 2){
			if (puzzle[9] == 1 && puzzle[5] == 1) {	
				puzzle[9] = 0; puzzle[5] = 0; puzzle[2]=1;
				p--;
				h++;
				saveMove = "(9,5,2)";
			
			} // END INNER IF !

				else if (puzzle[7] == 1 && puzzle[4] == 1){
					puzzle[7] = 0; puzzle[4] = 0; puzzle[2]=1;
					p--;
					h++;
					saveMove = "(7,4,2)";
 
				} // END ELSE IF

			} // END IF !

//*****************************************************************************************
			// IF 3 EMPTY // 
		if (point == 3){
			if (puzzle[5] == 1 && puzzle[4] == 1) {	
				puzzle[5] = 0; puzzle[4] = 0; puzzle[3]=1;
				p--;
				h++;
				saveMove = "(5,4,3)";
			
			} // END INNER IF !

				else if (puzzle[0] == 1 && puzzle[1] == 1){
					puzzle[0] = 0; puzzle[1] = 0; puzzle[3]=1;
					p--;
					h++;
					saveMove = "(0,1,3)";
				}
						else if (puzzle[7] == 1 && puzzle[12] == 1){
							puzzle[7] = 0; puzzle[12] = 0; puzzle[3]=1;
							p--;
							h++;
							saveMove = "(7,12,3)";
						}
								else if (puzzle[10] == 1 && puzzle[6] == 1){
									puzzle[10] = 0; puzzle[6] = 0; puzzle[3]=1;
									p--;
									h++;
									saveMove = "(10,6,3)";
								} // END ELSE IF
			
		} // END IF !

//*****************************************************************************************
				// IF 4 EMPTY // 
		if (point == 4){
			if (puzzle[8] == 1 && puzzle[13] == 1) {	
				puzzle[8] = 0; puzzle[13] = 0; puzzle[4]=1;
				p--;
				h++;
				saveMove = "(8,13,4)";
			
			} // END INNER IF !

				else if (puzzle[11] == 1 && puzzle[7] == 1){
					puzzle[11] = 0; puzzle[7] = 0; puzzle[4]=1;
					p--;
					h++;
					saveMove = "(11,7,4)";
 
				} // END ELSE IF

			} // END IF !

//*****************************************************************************************
			// IF 5  EMPTY // 
		if (point == 5){
			if (puzzle[9] == 1 && puzzle[14] == 1) {	
				puzzle[9] = 0; puzzle[14] = 0; puzzle[5]=1;
				p--;
				h++;
				saveMove = "(9,14,5)";
			
			} // END INNER IF !

				else if (puzzle[0] == 1 && puzzle[1] == 1){
					puzzle[0] = 0; puzzle[1] = 0; puzzle[5]=1;
					p--;
					h++;
					saveMove = "(0,1,5)";
				}
						else if (puzzle[3] == 1 && puzzle[4] == 1){
							puzzle[3] = 0; puzzle[4] = 0; puzzle[5]=1;
							p--;
							h++;
							saveMove = "(3,4,5)";
						}
								else if (puzzle[12] == 1 && puzzle[8] == 1){
									puzzle[12] = 0; puzzle[8] = 0; puzzle[5]=1;
									p--;
									h++;
									saveMove = "(12,8,5)";
								} // END ELSE IF
			
		} // END IF !

//*****************************************************************************************
				// IF 6 EMPTY // 
		if (point == 6){
			if (puzzle[1] == 1 && puzzle[3] == 1) {	
				puzzle[1] = 0; puzzle[3] = 0; puzzle[6]=1;
				p--;
				h++;
				saveMove = "(1,3,6)";
			
			} // END INNER IF !

				else if (puzzle[7] == 1 && puzzle[8] == 1){
					puzzle[7] = 0; puzzle[8] = 0; puzzle[6]=1;
					p--;
					h++;
					saveMove = "(7,8,6)";
 
				} // END ELSE IF

			} // END IF !

//*****************************************************************************************
				// IF 7 EMPTY // 
		if (point == 7){
			if (puzzle[8] == 1 && puzzle[9] == 1) {	
				puzzle[8] = 0; puzzle[9] = 0; puzzle[7]=1;
				p--;
				h++;
				saveMove = "(8,9,4)";
			
			} // END INNER IF !

				else if (puzzle[2] == 1 && puzzle[4] == 1){
					puzzle[2] = 0; puzzle[4] = 0; puzzle[7]=1;
					p--;
					h++;
					saveMove = "(2,4,7)";
 
				} // END ELSE IF

			} // END IF !

//*****************************************************************************************
				// IF 8 EMPTY // 
		if (point == 8){
			if (puzzle[6] == 1 && puzzle[7] == 1) {	
				puzzle[6] = 0; puzzle[7] = 0; puzzle[8]=1;
				p--;
				h++;
				saveMove = "(6,7,8)";
			
			} // END INNER IF !

				else if (puzzle[1] == 1 && puzzle[4] == 1){
					puzzle[1] = 0; puzzle[4] = 0; puzzle[8]=1;
					p--;
					h++;
					saveMove = "(1,4,8)";
 
				} // END ELSE IF

			} // END IF !

//*****************************************************************************************
				// IF 9 EMPTY // 
		if (point == 9){
			if (puzzle[8] == 1 && puzzle[7] == 1) {	
				puzzle[8] = 0; puzzle[7] = 0; puzzle[9]=1;
				p--;
				h++;
				saveMove = "(8,7,9)";
			
			} // END INNER IF !

				else if (puzzle[2] == 1 && puzzle[5] == 1){
					puzzle[2] = 0; puzzle[5] = 0; puzzle[9]=1;
					p--;
					h++;
					saveMove = "(2,5,9)";
 
				} // END ELSE IF

			} // END IF !




//*****************************************************************************************
				// IF 10 EMPTY // 
		if (point == 10){
			if (puzzle[3] == 1 && puzzle[6] == 1) {	
				puzzle[3] = 0; puzzle[6] = 0; puzzle[10]=1;
				p--;
				h++;
				saveMove = "(3,6,10)";
			
			} // END INNER IF !

				else if (puzzle[12] == 1 && puzzle[11] == 1){
					puzzle[12] = 0; puzzle[11] = 0; puzzle[10]=1;
					p--;
					h++;
					saveMove = "(12,11,10)";
 
				} // END ELSE IF

			} // END IF !

//*****************************************************************************************
				// IF 11 EMPTY // 
		if (point == 11){
			if (puzzle[4] == 1 && puzzle[7] == 1) {	
				puzzle[4] = 0; puzzle[7] = 0; puzzle[11]=1;
				p--;
				h++;
				saveMove = "(4,7,11)";
			
			} // END INNER IF !

				else if (puzzle[12] == 1 && puzzle[13] == 1){
					puzzle[12] = 0; puzzle[13] = 0; puzzle[11]=1;
					p--;
					h++;
					saveMove = "(12,13,4)";
 
				} // END ELSE IF

			} // END IF !

//*****************************************************************************************
				// IF 12 EMPTY // 
		if (point == 12){
			if (puzzle[10] == 1 && puzzle[11] == 1) {	
				puzzle[10] = 0; puzzle[11] = 0; puzzle[12]=1;
				p--;
				h++;
				saveMove = "(10,11,12)";
			
			} // END INNER IF !

				else if (puzzle[13] == 1 && puzzle[14] == 1){
					puzzle[13] = 0; puzzle[14] = 0; puzzle[12]=1;
					p--;
					h++;
					saveMove = "(13,14,12)";
				}
						else if (puzzle[8] == 1 && puzzle[5] == 1){
							puzzle[8] = 0; puzzle[5] = 0; puzzle[12]=1;
							p--;
							h++;
							saveMove = "(8,5,12)";
						}
								else if (puzzle[7] == 1 && puzzle[3] == 1){
									puzzle[7] = 0; puzzle[3] = 0; puzzle[12]=1;
									p--;
									h++;
									saveMove = "(7,3,12)";
								} // END ELSE IF
			
		} // END IF !


//*****************************************************************************************
				// IF 13 EMPTY // 
		if (point == 13){
			if (puzzle[11] == 1 && puzzle[12] == 1) {	
				puzzle[11] = 0; puzzle[12] = 0; puzzle[13]=1;
				p--;
				h++;
				saveMove = "(11,12,13)";
			
			} // END INNER IF !

				else if (puzzle[4] == 1 && puzzle[8] == 1){
					puzzle[4] = 0; puzzle[8] = 0; puzzle[13]=1;
					p--;
					h++;
					saveMove = "(4,8,13)";
 
				} // END ELSE IF

			} // END IF !

//*****************************************************************************************
				// IF 14 EMPTY /0/ 
		if (point == 14){
			if (puzzle[12] == 1 && puzzle[13] == 1) {	
				puzzle[12] = 0; puzzle[13] = 0; puzzle[14]=1;
				p--;
				h++;
				saveMove = "(12,13,14)";
			
			} // END INNER IF !

				else if (puzzle[5] == 1 && puzzle[9] == 1){
					puzzle[5] = 0; puzzle[9] = 0; puzzle[14]=1;
					p--;
					h++;
					saveMove = "(5,9,14)";
 
				} // END ELSE IF

			} // END IF !
		return saveMove;
	} // END MOVE METHOD !
} // end class HiQ
