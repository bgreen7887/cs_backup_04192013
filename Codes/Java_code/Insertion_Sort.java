/* 
Bilal Green
Programming Assignment 1
Insertion Sorting
*/ 
 import java.util.Random;
 import java.util.Arrays;

 public class Insertion_Sort {
 	public static int select = 0;
 	public static int comparcount = 0;
 	public static void main(String[] args) {
	//****************************** Random-Case Scenario //******************************//
/*	
	int[] randArray = new int[32];
	int i = 0;
	int j = 0;

	System.out.println("                       ** The Number Of Elements Is Equal To: " + randArray.length + " **" + "\n " ); 
	for(i=0;i<=randArray.length-1;i++) {
		randArray[i] =(int)(Math.random()*100);
	} 
	System.out.print("Rand-Case Array Unsorted ---> ");
	System.out.print("[ ");
	for(i=0;i<=randArray.length-1;i++) {
		System.out.print(randArray[i]);  
		System.out.print(" ");                        
	}
	System.out.print("]");
	System.out.print("\n");
	srt(randArray,randArray.length);
	
	System.out.print("Rand-Case Array Sorted ---> ");	
	System.out.print("[ ");	
	for(i=0;i<=randArray.length-1;i++) {
		System.out.print(randArray[i]);
		System.out.print(" ");
	}
	System.out.println("]");
	System.out.println();
*/
//****************************** Best-Case Scenario //******************************//
	/*int[] bestArray = new int[32];
	int i = 0;
	int j = 0;	
	for(i=0;i<=bestArray.length-1;i++) {
		bestArray[i] = i;
	} 
	srt(bestArray,bestArray.length);
		
	System.out.print("Best-Case Array Sorted ---> ");
	System.out.print("[ ");
	for(i=0;i<=bestArray.length-1;i++) {
		System.out.print(bestArray[i]);
		System.out.print(" ");
	}
	System.out.println("]");	
	System.out.println();

	*/	
	//***************************** Worst-Case Scenario //******************************
	int[] worstArray = {5,4,3,2,1,};
	int i = 0;
	int j = 0;
	/*for(i=0;i<=worstArray.length-1;i++) {
		worstArray[worstArray.length -i-1] = i;
	} 
	System.out.print("Wrst-Case Array Unsorted ---> ");
	System.out.print("[ ");
	for(i=0;i<=worstArray.length-1;i++) {
		System.out.print(worstArray[i]);  
		System.out.print(" ");                        
	}
	System.out.println("]");	*/
	srt(worstArray,worstArray.length);	
	System.out.print("Wrst-Case Array Sorted ---> ");
	System.out.print("[ ");		
	for(i=0;i<=worstArray.length-1;i++) {
		System.out.print(worstArray[i]);
		System.out.print(" ");
	}
	System.out.println("]");	
		System.out.println();
		
  } // end of main method
  
 public static void srt(int[] size, int n) {
 	for (int i =0; i<n; i++) {
 		select = size[i];
 		int j = i;
 		while((j>0) && Smaller(size[j-1])) {	
 			size [j] = size [j-1];
 			j--;		
 		}
 		size[j] = select;
 		}
 	System.out.print("# Of Key Comparisons ---> " + comparcount);	
 	System.out.println("");	
 	
 	}
 		public static boolean Smaller(int x) {
		comparcount ++;
		if (x > select) 
			return true;
		return false;
		
	}
 	

 } // class Insertion Sort

