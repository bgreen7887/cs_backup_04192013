public class KlinDriver 
{
	public static void main(String[] args)

	{
	Klingon kal = new Klingon("South-East Asia","Anything",73,"Ethan Suntag",2089,01176,"Rich Gannon",4,2890,5);
		System.out.println("Original Origin: " + kal.getOrigin());
		kal.setOrigin("Bahamma Mamma");
		System.out.println("After calling the method: " + kal.getOrigin());
		System.out.println("");
		System.out.println("Starting out number of pages " +kal.getPages());
		kal.setPages(67);
		System.out.println("After calling the method: "+kal.getPages());
		System.out.println("");
		 System.out.println("First Author: "+kal.getAuthor());
		 kal.setAuthor("Andrew Sohn");
		System.out.println("After call the method: " +kal.getAuthor());
		System.out.println("");
		System.out.println("Natural dialect: "+kal.getDialect());
		kal.setDialect("Newaropean");
		System.out.println("After call the method: " + kal.getDialect());
		System.out.println("");
		System.out.println("First realease year: "+kal.getYear());
		kal.setYear(1987);
		System.out.println("After call the method: " + kal.getYear());
		System.out.println("");
		System.out.println("Original ISBN: "+kal.getIsbn());
		kal.setIsbn(98761);
		System.out.println("After call the method: " + kal.getIsbn());
		System.out.println("");
		System.out.println("Volume released First: "+kal.getVolume());
		kal.setVolume(7);
		System.out.println("After call the method: " + kal.getVolume());
		System.out.println("");
		System.out.println("Number of Definitions: "+kal.getDefinitions());
		kal.setDefinitions(1980);
		System.out.println("After call the method: " + kal.getDefinitions());
		System.out.println("");






	}
}