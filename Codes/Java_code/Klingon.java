public class Klingon extends Dictionary
{
	protected String origin;
	protected String dialect;
	
	public Klingon(String origin,String dialect,int pages,String publisher,int year,int isbn,String author,int volume,int definitions,int edition)
	{
	super (pages, publisher,year,isbn, author, volume,definitions,edition);
	this.origin = origin;
	this.dialect = dialect;
	
	
	}
	

			public String getOrigin()
			{
			return origin;
			}
			
			
			public void  setOrigin(String origin)
			{
			this.origin = origin;
			
			}
			
			//
			
			public String getDialect()
			{
			return dialect;
			}
			
			
			public void  setDialect(String dialect)
			{
			this.dialect = dialect;
			
			}
			
			
}


