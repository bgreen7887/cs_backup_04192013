import javax.swing.JOptionPane;
 public class MathLearning 
 {
	public static void main( String[] args )
	{
	int runs = 0;
	String output = "";
	int tally = 0;
	

		while ( runs <= 3 ) 
		{
		int number1 = (int)(Math.random() * 100);
		int number2 = (int)(Math.random() * 100);
	
		String answerString = JOptionPane.showInputDialog( " What is " + number1 + "+" + number2 );

		int answer = Integer.parseInt( answerString );

		if (answer == number1 + number2)
		  {
			JOptionPane.showMessageDialog(null, "You are correct" ); 				tally ++;
			
		  } else
 			JOptionPane.showMessageDialog(null, "You are wrong");

		runs ++;

		output += "\n" + number1 + "+" + number2 + " = " + answerString + ((number1 + number2 == answer) ? " correct " : " wrong");
	
		} // end WHILE LOOP !
		
	JOptionPane.showMessageDialog(null, output);
	JOptionPane.showMessageDialog(null, (tally/runs) + "%");		

	} // end main method
 } // end class MathLearning.java
