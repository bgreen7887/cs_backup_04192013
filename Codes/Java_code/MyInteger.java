public class swapJava{	
	public static void main(String[] args) {	
		Integer x = new Integer(8);
		Integer y = new Integer(3);
		System.out.println("Before Swap");
		System.out.println(" x = [ " + x + " ]");
		System.out.println(" y = [ " + y + " ]");
		
		Integer temp = x;
		x = y;
		y = temp;
		System.out.println("After Swap");
		System.out.println(" x = [ " + x + " ]");
		System.out.println(" y = [ " + y + " ]");
		
	}	
}	
