public class PascalsTriangle {

public static void main(String[] args) {
int numbersList[] = new int[0];
displayNthLine(numbersList, 9);
}

static void displayNthLine(int origList[], int desiredLine) {
int newList[] = new int[origList.length + 1];
newList[0] = 1; // first num is always a 1

// calculate the new numbs
for (int i = 1; i < origList.length; i++)
newList[i] = origList[i - 1] + origList[i];

newList[newList.length - 1] = 1; for (int i = 0; i < newList.length; i++)
System.out.print(newList[i] + " ");

System.out.println(); 
if (newList.length < desiredLine)
displayNthLine(newList, desiredLine);
}
}
