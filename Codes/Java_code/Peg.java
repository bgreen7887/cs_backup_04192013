import java.util.Scanner;
public class Peg {	 

		private  static int [] pegs = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
		private static String[] solution = new String[15];
		private  static int into, over, from,max,remainingPegs;
		private  static boolean move;
		


		//getters and setters for each variable;
		public void setInto(int i){
		into = i;
		}

		public int getInto(){
		return into;
		}

		public void setOver(int o){
		over = o;
		}

		public int getOver(){
		return into;
		}
		
		public void setFrom(int f){
		from = f;
		}

		public int getFrom(){
		return from;
		}

		public void setMax(int m){
		max = m;
		}

		public int getMax(){
		return max;
		}

		public void setRemainingPegs(int r){
		remainingPegs = r;
		}

		public int getRemainingPegs(){
		return remainingPegs;
		}

/*
	findsolution(Peg p){
		if(p.remainingpegs==goal){
			print solution
			
		}
	}

*/

		// ALL POSSIBLE VAILD MOVES
		private static int[][] possibleMoves = new int[][]{	
				// "OVER, FROM"
			{1,3,	2,5,	0,0,	0,0},	//Empty 0
			{3,6,	4,8,	0,0,	0,0},	//Empty 1
			{5,9,	4,7,	0,0,	0,0},	//Empty 2
			{6,10,	7,12,	4,5,	1,0},	//Empty	3
			{7,11,	8,13,	0,0,	0,0},	//Empty 4
			{9,14,	2,0,	4,3,	8,12},	//Empty 5
			{3,1,	7,8,	0,0,	0,0},	//Empty 6
			{4,2,	8,9,	0,0,	0,0},	//Empty 7
			{7,6,	4,1,	0,0,	0,0},	//Empty 8
			{5,2,	8,7,	0,0,	0,0},	//Empty 9
			{6,3,	11,12,	0,0,	0,0},	//Empty10
			{7,4,	12,13,	0,0,	0,0},	//Empty11
			{11,10,	13,14,	8,5,	7,3},	//Empty12
			{12,11,	8,4,	0,0,	0,0},	//Empty13
			{13,12,	9,5,	0,0,	0,0},	//Empty14
			};	


	
	
		//FUNCTION TO SHOW BOARD
		public static void showBoard() {
			System.out.print("	  " + pegs[0] + "\n");
			System.out.print("	"+pegs[1]+"   "+pegs[2] + "\n");
			System.out.print("      "+pegs[3]+"   "+pegs[4]+"   "+pegs[5] +"\n");
			System.out.print("    "+pegs[6]+"   "+pegs[7]+"   "+pegs[8]+"   "+pegs[9]+"\n");
			System.out.print("  "+pegs[10]+"   "+pegs[11]+"   "+pegs[12]+"   "+pegs[13]+"   "+pegs[14]+"\n" );
		}
	
		// SETUP BOARD FUNCTION
		public static void setupBoard() {
		
			Scanner scan = new Scanner(System.in);		
		
			System.out.print("Which Peg to leave Empty? :");
			int emptyPeg;
			emptyPeg = scan.nextInt();
			pegs[emptyPeg]=0;
			System.out.print("What's Number of Acceptable Pegs left? ");		
			max = scan.nextInt();
		
		}
	
		// CHECK MOVE FUNCTON
		public static boolean moveCheck(int into, int over, int from) {
	 	if (over == possibleMoves[into][0] && from == possibleMoves[into][1])      move = true;
     			else if (over == possibleMoves[into][2] && from == possibleMoves[into][3])     move = true;
     				else if (over == possibleMoves[into][4] && from == possibleMoves[into][5])     move = true;
     					else if (over == possibleMoves[into][6] && from == possibleMoves[into][7])     move = true;
    						 else move = false;
							return (pegs[into]==0 && pegs[over]==1 && pegs[from] ==1 && move);
		}

		public static void main(String[] args){
			Peg start = new Peg();
			start.setupBoard();
			Scanner scan = new Scanner(System.in);
			int remainingPegs = 14;
			//setupBoard();
			boolean contin = true;
			int turn = 0;
			//showBoard();
			System.out.print("DO YOU WANT TO SOLVE INTERACTIVELY OR RECURSIVELY??" + "\n1: Interactively Solve: "+"\n2: Recursive Solution: \n");			
			int choose = scan.nextInt();
			if (choose == 1){
			    setupBoard();
			   showBoard();

	   		do  {	
				turn = turn +1;
				System.out.println("Please pick an 'Into' space: ");
				into = scan.nextInt();
				System.out.println("");

				System.out.println("Please pick an 'Over' space: ");
				over = scan.nextInt();
				System.out.println("");

				System.out.println("Please pick an 'From' space: ");
				from = scan.nextInt();
				System.out.println("");
	   			  if (moveCheck(into, over, from) == true){
					remainingPegs = remainingPegs -1;
	      				// UPDATE PEGS
					pegs[into] = 1;
					pegs[over] = 0;
					pegs[from] = 0;
		 			System.out.println("Nice, your move has been completed");
					showBoard();
					System.out.print("You've just completed your " + turn +" turn "+ " Would you like to continue ?\n"+"Please enter <true/false> : ");
					System.out.print("");
					contin = scan.nextBoolean();
		
				}// end 'if' statement
					else {		
						System.out.println("Sorry, thats an invalid move. Please re-try");
						turn = turn -1;
		     		     	     }// end 'else' statement
			}// end 'do' 

			while(contin != false);{}
		}// end 'choose if'
					
	}
}
