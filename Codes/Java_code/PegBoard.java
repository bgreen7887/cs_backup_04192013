public class PegBoard {

	public static int[] pegs = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
	public static int into,over,from,max,remainingPegs,found; 
	public static boolean move;

	public PegBoard(int hole, int maxPeg){			// CONSTRUCTOR 
		pegs[hole]=0;
		maxPeg = max ;
		remainingPegs=14;
	}

	public int findSolution(PegBoard p){
		if(remainingPegs == max){
			System.out.println("You have reached your solution");
			p.showBoard();
			
		}else{
			if(moveCheck(into,over,from) == true) {
				into = 1;
				over = 0;
				from =0;
				found = findSolution(p);
				return 0;
			
			}
		
		     }
		
				return 0;
	}
		
				// ALL POSSIBLE VAILD MOVES
		private static int[][] possibleMoves = new int[][]{	
				// "OVER, FROM"
			{1,3,	2,5,	0,0,	0,0},	//Empty 0
			{3,6,	4,8,	0,0,	0,0},	//Empty 1
			{5,9,	4,7,	0,0,	0,0},	//Empty 2
			{6,10,	7,12,	4,5,	1,0},	//Empty	3
			{7,11,	8,13,	0,0,	0,0},	//Empty 4
			{9,14,	2,0,	4,3,	8,12},	//Empty 5
			{3,1,	7,8,	0,0,	0,0},	//Empty 6
			{4,2,	8,9,	0,0,	0,0},	//Empty 7
			{7,6,	4,1,	0,0,	0,0},	//Empty 8
			{5,2,	8,7,	0,0,	0,0},	//Empty 9
			{6,3,	11,12,	0,0,	0,0},	//Empty10
			{7,4,	12,13,	0,0,	0,0},	//Empty11
			{11,10,	13,14,	8,5,	7,3},	//Empty12
			{12,11,	8,4,	0,0,	0,0},	//Empty13
			{13,12,	9,5,	0,0,	0,0},	//Empty14
			};	
	
				//FUNCTION TO SHOW BOARD
		public static void showBoard() {
			System.out.print("	  " + pegs[0] + "\n");
			System.out.print("	"+pegs[1]+"   "+pegs[2] + "\n");
			System.out.print("      "+pegs[3]+"   "+pegs[4]+"   "+pegs[5] +"\n");
			System.out.print("    "+pegs[6]+"   "+pegs[7]+"   "+pegs[8]+"   "+pegs[9]+"\n");
			System.out.print("  "+pegs[10]+"   "+pegs[11]+"   "+pegs[12]+"   "+pegs[13]+"   "+pegs[14]+"\n" );
		}
				// CHECK MOVE FUNCTON
		public static boolean moveCheck(int into, int over, int from) {
	 	if (over == possibleMoves[into][0] && from == possibleMoves[into][1])      move = true;
     			else if (over == possibleMoves[into][2] && from == possibleMoves[into][3])     move = true;
     				else if (over == possibleMoves[into][4] && from == possibleMoves[into][5])     move = true;
     					else if (over == possibleMoves[into][6] && from == possibleMoves[into][7])     move = true;
    						 else move = false;
							return (pegs[into]==0 && pegs[over]==1 && pegs[from] ==1 && move);
		}

				// GETTERS AND SETTERS FOR EACH METHOD 
		public void setInto(int i){
		into = i;
		}

		public int getInto(){
		return into;
		}

		public void setOver(int o){
		over = o;
		}

		public int getOver(){
		return into;
		}
		
		public void setFrom(int f){
		from = f;
		}

		public int getFrom(){
		return from;
		}

		public void setMax(int m){
		max = m;
		}

		public int getMax(){
		return max;
		}

		public void setRemainingPegs(int r){
		remainingPegs = r;
		}

		public int getRemainingPegs(){
		return remainingPegs;
		}




}
