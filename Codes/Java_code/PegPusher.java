import java.util.Scanner;
public class PegPusher{

	
	static public Move[] moves_done = new Move[13];
	static public Move[] possibleMoves = {new Move(0,1,3),new Move(0,2,5),new Move(1,3,6),new Move(1,4,8),new Move(2,4,7),new Move(2,5,9),new Move(3,1,0),new Move(3,4,5),new Move(3,6,10),new Move(3,7,12),new Move(4,8,13),new Move(4,7,11),new Move(5,2,0),new Move(5,4,3),new Move(5,8,12),new Move(5,9,14),new Move(6,3,1),new Move(6,7,8),new Move(7,4,2),new Move(7,8,9),new Move(8,4,1),new Move(8,7,6),new Move(9,5,2),new Move(9,8,7),new Move(10,6,3),new Move(10,11,12),new Move(11,7,4),new Move(11,12,13),new Move(12,7,3),new Move(12,8,5),new Move(12,11,10),new Move(12,13,14),new Move(13,8,4),new Move(13,12,11),new Move(14,9,5),new Move(14,13,12)};

	  public int [] board = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

	public int movesCount = 0;
	public int empty_pegs = 0;
	public int rem_pegs;

	public PegPusher(){
		for (int i = 0; i<13; i++){
			moves_done[i] = new Move(0, 0, 0);
		}
	}

	public boolean findMove ()    //FIND MOVE FUNCTION 
{
	if (rem_pegs >= (14 - movesCount)) return true;    //CHECK IF SOLUTION HAS BEEN MET       

    for (int i=0;i<36;i++)    //CHECK FOR POSSIBLE MOVES
    {
	//System.out.println("E:"+possibleMoves[i].into); 
        if (board[possibleMoves[i].into] == 0 && board[possibleMoves[i].over] == 1 && board[possibleMoves[i].from] == 1)    //CHECK FOR VALID MOVE
        {
            //MAKE MOVE
		
            board[possibleMoves[i].into] = 1;
            board[possibleMoves[i].over] = 0;
            board[possibleMoves[i].from] = 0;
            
		//System.out.println("E:"+possibleMoves[i].into+"I:"+moves_done[mov]); 
            //STORE IN MOVE HISTORY
            moves_done[movesCount].into = possibleMoves[i].into;
            moves_done[movesCount].over = possibleMoves[i].over;
            moves_done[movesCount].from = possibleMoves[i].from;
            movesCount++;    //UPDATE MOVE INDEX

			if ( findMove() == false ) 	
			{
        		movesCount--;    //BACKTRACK
        		board[moves_done[movesCount].into] = 0;
		        board[moves_done[movesCount].over] = 1;
        		board[moves_done[movesCount].from] = 1;
            }
			else return true;
        }   
    }
    //IF PROGRAM REACHES HERE, THERE ARE NO MORE MOVES TO MAKE 
	  //System.out.println("E"); 
   	return false;
}

	public static void main(String[] args) {
	PegPusher pp = new PegPusher();
	Scanner scan = new Scanner(System.in);
	System.out.print("Which starting peg would you like to leave Empty(1-14): ");
	pp.empty_pegs = scan.nextInt();
	pp.board[pp.empty_pegs] = 0;
	System.out.print("Whats amount of remaining pegs to win (1-14): ");
	pp.rem_pegs = scan.nextInt();

	if (pp.findMove()) {
		System.out.println("Solution Found - ");
		System.out.println("Moves made: " + pp.movesCount);

			for(int k = 0; k<13; k++) {

				System.out.println(moves_done[k].into + " " + moves_done[k].over + " " + moves_done[k].from);
			}
	}
	else System.out.println("There is no possible solution");
  }

 }
