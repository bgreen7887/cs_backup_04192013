import java.util.Scanner;
public class PegSolitaireGame{

	
	static public Move[] completedMoves = new Move[13];
	static public Move[] possibleMoves = {new Move(0,1,3),new Move(0,2,5),new Move(1,3,6),
						new Move(1,4,8),new Move(2,4,7),new Move(2,5,9),
						new Move(3,1,0),new Move(3,4,5),new Move(3,6,10),
						new Move(3,7,12),new Move(4,8,13),new Move(4,7,11),
						new Move(5,2,0),new Move(5,4,3),new Move(5,8,12),
						new Move(5,9,14),new Move(6,3,1),new Move(6,7,8),
						new Move(7,4,2),new Move(7,8,9),new Move(8,4,1),
						new Move(8,7,6),new Move(9,5,2),new Move(9,8,7),
						new Move(10,6,3),new Move(10,11,12),new Move(11,7,4),
						new Move(11,12,13),new Move(12,7,3),new Move(12,8,5),
						new Move(12,11,10),new Move(12,13,14),new Move(13,8,4),
						new Move(13,12,11),new Move(14,9,5),new Move(14,13,12)};

	  public int [] board = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

	public int movesCount = 0;
	public int emptyPegs = 0;
	public int remPegs;

	public PegSolitaireGame(){
		for (int i = 0; i<13; i++){
			completedMoves[i] = new Move(0, 0, 0);
		}
	}

	public boolean solve ()    //FIND MOVE FUNCTION 
{
	if (remPegs >= (14 - movesCount)) return true;    //CHECK IF SOLUTION HAS BEEN MET       

    for (int i=0;i<36;i++)    //CHECK FOR POSSIBLE MOVES
    {
	//System.out.println("E:"+possibleMoves[i].into); 
        if (board[possibleMoves[i].into] == 0 && board[possibleMoves[i].over] == 1 && board[possibleMoves[i].from] == 1)    //CHECK FOR VALID MOVE
        {
            //MAKE MOVE
		
            board[possibleMoves[i].into] = 1;
            board[possibleMoves[i].over] = 0;
            board[possibleMoves[i].from] = 0;
            
		//System.out.println("E:"+possibleMoves[i].into+"I:"+completedMoves[mov]); 
            //STORE IN MOVE HISTORY
            completedMoves[movesCount].into = possibleMoves[i].into;
            completedMoves[movesCount].over = possibleMoves[i].over;
            completedMoves[movesCount].from = possibleMoves[i].from;
            movesCount++;    //UPDATE MOVE INDEX

			if ( solve() == false ) 	
			{
        		movesCount--;    //BACKTRACK
        		board[completedMoves[movesCount].into] = 0;
		        board[completedMoves[movesCount].over] = 1;
        		board[completedMoves[movesCount].from] = 1;
            }
			else return true;
        }   
    }
    //IF PROGRAM REACHES HERE, THERE ARE NO MORE MOVES TO MAKE 
	  //System.out.println("E"); 
   	return false;
}

	public static void main(String[] args) {
	PegSolitaireGame push = new PegSolitaireGame();
	Scanner scan = new Scanner(System.in);
	System.out.print("Which starting peg would you like to leave Empty(1-14): ");
	push.emptyPegs = scan.nextInt();
	push.board[push.emptyPegs] = 0;
	System.out.print("Whats amount of remaining pegs to win (1-14): ");
	push.remPegs = scan.nextInt();

	if (push.solve()) {
		System.out.println("** W I N N N E R +\n" +" Game Solved !! " +" ** ");
		System.out.println("Moves made: " + push.movesCount);

			for(int k = 0; k<13; k++) {

				System.out.println(completedMoves[k].into + " " + completedMoves[k].over + " " + completedMoves[k].from);
			}
	}
	else System.out.println("There is no possible solution");
  }

 }
