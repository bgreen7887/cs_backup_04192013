import java.util.Scanner;
public class PegSolitaireGame{

	
	static public Jump[] completedMoves = new Jump[13];
	static public Jump[] possibleMoves = {new Jump(0,1,3),new Jump(0,2,5),new Jump(1,3,6),
						new Jump(1,4,8),new Jump(2,4,7),new Jump(2,5,9),
						new Jump(3,1,0),new Jump(3,4,5),new Jump(3,6,10),
						new Jump(3,7,12),new Jump(4,8,13),new Jump(4,7,11),
						new Jump(5,2,0),new Jump(5,4,3),new Jump(5,8,12),
						new Jump(5,9,14),new Jump(6,3,1),new Jump(6,7,8),
						new Jump(7,4,2),new Jump(7,8,9),new Jump(8,4,1),
						new Jump(8,7,6),new Jump(9,5,2),new Jump(9,8,7),
						new Jump(10,6,3),new Jump(10,11,12),new Jump(11,7,4),
						new Jump(11,12,13),new Jump(12,7,3),new Jump(12,8,5),
						new Jump(12,11,10),new Jump(12,13,14),new Jump(13,8,4),
						new Jump(13,12,11),new Jump(14,9,5),new Jump(14,13,12)};

	public int [] pegBoard = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
	public int movesCount = 0;
	public int emptyPegs = 0;
	public int remPegs;

	public PegSolitaireGame(){
		for (int i = 0; i<13; i++){
			completedMoves[i] = new Jump(0, 0, 0);
		}
	}

	public boolean solve (){

	if (remPegs >= (14 - movesCount)) return true;          

    for (int i=0;i<36;i++)    
    {
	
        if (pegBoard[possibleMoves[i].into] == 0 && pegBoard[possibleMoves[i].over] == 1 && pegBoard[possibleMoves[i].from] == 1)  
        {
           
		
            pegBoard[possibleMoves[i].into] = 1;
            pegBoard[possibleMoves[i].over] = 0;
            pegBoard[possibleMoves[i].from] = 0;
            
            completedMoves[movesCount].into = possibleMoves[i].into;
            completedMoves[movesCount].over = possibleMoves[i].over;
            completedMoves[movesCount].from = possibleMoves[i].from;
            movesCount++;  

			if ( solve() == false ) 	
			{
        		movesCount--;    
        		pegBoard[completedMoves[movesCount].into] = 0;
		        pegBoard[completedMoves[movesCount].over] = 1;
        		pegBoard[completedMoves[movesCount].from] = 1;
            }
			else return true;
        }   
    }
   
   	return false;
}

	public static void main(String[] args) {
	PegSolitaireGame push = new PegSolitaireGame();
	Scanner scan = new Scanner(System.in);
	System.out.print("Which starting peg would you like to leave Empty(1-14): ");
	push.emptyPegs = scan.nextInt();
	push.pegBoard[push.emptyPegs] = 0;
	System.out.print("Whats amount of remaining pegs to win (1-14): ");
	push.remPegs = scan.nextInt();

	if (push.solve()) {
		System.out.println("** W I N N N E R ** \n "+"**** Game Solved !! " +"**** ");
		System.out.println("It only took me  " + push.movesCount + " moves !!");
			System.out.println("INTO | OVER | FROM");
			for(int k = 0; k<13; k++) {
				System.out.println(completedMoves[k].into + " 	"+ completedMoves[k].over + " 	" + completedMoves[k].from);
			}
		}
	else System.out.println("No Solutions Available: ");
  	}


 }
