 public class Point 

{
	
	// attributes 
	
	private double myX = 0.0;
	private double myY = 0.0;

	// constructor

	public Point(double x,double y) 
	  {
	     myX = x;
	     myY = y;
	  }
		
	public double getX()
	  {
	     return myX; 		
		
	  } 
	public double getY()
	  {
	     return myY; 		
		
	  }

	public void setPoint(double x, double y)
	  {
	     myX = x;
	     myY = y; 		
		
	  }
	
}
