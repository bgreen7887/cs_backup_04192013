/*
Bilal Green Programming Project 2
Recursive decent parser

A -> I = E
E -> T + E | T - E | T
T -> F * T | F / T | F
F -> P ^ F | P
P -> I | L | (E)
I -> a | b | ... | y | z
L -> 0 | 1 | ... | 8 | 9

*/
import java.io.*;
import java.util.*;

public class Project_Two {
	public static void main(String[] args ) {
	
		File in = new File("input.txt");
    	        Scanner scan = null;
    	        try {
    	             scan = new Scanner(in);
    	             	while (scan.hasNext()) {
    	             		s= scan.nextLine();
    	             		if (A() == true) {
    	             		System.out.println("The string read from \"input.txt\" : " + s + " is in the language");
    	             		}
    	             		else {
    	             		System.out.println("The string read from \"input.txt \":  " + s + " is not in language");
    	             		}
    	             	}
    	             	}
    	        
    	    catch (FileNotFoundException e) {
            	System.exit(0);
        }
			}
	
	// assignment equal identifier equals expression
	
	private static String s;
	private static int i; 
	
	private static boolean A() {
		if(I()) {
			if(i<s.length() && (s.charAt(i) == '=')) {
			++i;
				if (E()) {
					return true;
					} else {
					return false;
					}
					}
					return true;
					}
					return false;
					}
					
	private static boolean E() {
		if (T()) {
			if(i<s.length() && (s.charAt(i) == '+' || s.charAt(i) == '-')) {
				++i;
				if(E()) {
					return true;
					}
				else {
					return false;
					}
				}
				return true;
				}
				return false;
			}

	private static boolean T() {
		if (F()) {
			if (i<s.length() && (s.charAt(i) == '*' || s.charAt(i) == '/')) {
				++i;
				if(T()) {
					return true;
					}
				else {
					return false;
					}
					
				}
				return true;
				}
				return false;
				}
				
	private static boolean F() {
		if (P()) {
			if(i<s.length() && (s.charAt(i) == '^')) {
			 	++i;
			 	if(F()) {
			 		return true;
			 	}
			 	else {
			 		return false;
			 		}
			}
			return true;
			}
			return false;
			}
			
	private static boolean P() {
		if (I()) {
			return true;
			}
		else if(L()) {
			return true;
			}
		else if (i<s.length() && (s.charAt(i) == '(')) {
				++i;
				if (E()) {
					if (i<s.length() && s.charAt(i) == ')') {
					++i;
					return true;
					}
					}
					}
					return false;
					}
					
	private static boolean I() {
		if(s.charAt(i) >= 'a' && s.charAt(i) <= 'z'){
			++i;
			return true;
			}
			return false;
			}
					
	private static boolean L() {
		if(i<s.length() && s.charAt(i) >= '0' && s.charAt(i) <='9') {
			i ++;
			return true;
			}
			return false;
			}
			} // end Project_Two class
			
				
					








