import java.lang.Math;

public class Rectangle extends Shape {
private double height;
private double width;

public Rectangle(Point org, double h, double w)
{
    super(org);
    height = h;
    width = w;
}
public double Area()
{
    return height * width;
}
public double Perimeter() 
{
    return 2 * height + 2 * width;   
}
public void setHeight(double h)
{
    height = h;
}
public void setWidth(double w)
{
    width = w;
}
public double getHeight()
{
    return height;
}
public double getWidth()
{
    return width;
}
public void setWH(double w, double h)
{
    width = w; height = h;
}



}
