import java.util.Scanner;

public class Seconds {

	public static void main(String []args) {
	Scanner scan = new Scanner(System.in);

	System.out.println("Enter amount of seconds: ");

	int sec = scan.nextInt();

	int hours = sec / 3600;
	sec = sec % 3600;
	int min = sec / 60;
	sec = sec % 60;

	System.out.println("That is exactly: " + hours + " hours\n" + min + " minutes\n" + sec + " seconds." );
	}
}
