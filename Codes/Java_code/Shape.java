public abstract class Shape {

// Attributes //
private Color color;
private Point origin;

// Constructors //
protected Shape(Color col, Point org)
 {
   origin = new Point(org.getX(),org.getY());
   color = new Color(col.getRed(), col.getBlue(), col.getGreen());
 }
protected Shape(Point org)
 {
   origin = new Point(org.getX(),org.getY());
   color = new Color(0,0,0); // black by default
 }
protected Shape()
 {
   origin = new Point(0.0, 0.0);
   color = new Color(0,0,0); // black by default
 }
// abstract methods //
public abstract double Area();
public abstract double Perimeter();

// getter and setters //

public Color getColor()
 {
    return color;
 }
public void setColor(Color col)
 {
    color.setRGB(col.getRed(), col.getGreen(), col.getBlue());
 }
public Point getOrigin()
 {
    return origin;
 }
public void setOrigin(Point org)
 {
    origin.setPoint(org.getX(), org.getY());
 }
}

