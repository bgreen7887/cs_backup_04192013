import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SimpleSwing {

private static JFrame myFrame;
private static JPanel myPanel;
private JButton button1;
private JButton button2;

public SimpleSwing() {              // Construct build gui
    myPanel = new JPanel();
    button1 = new JButton("Button 1 "); // Creates buttons
    button2 = new JButton("Button 2 ");
 // set action listener for both buttons !!
 

    button1.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
       JOptionPane.showMessageDialog(myFrame, "Button 1 pressed");
}
 });
        button2.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
       JOptionPane.showMessageDialog(myFrame, "Button 2 pressed");
}
 });

    myPanel.add(button1);  // adds to current JFrame
    myPanel.add(button2);  
    
   
}

public static void main(String[] args) {
SimpleSwing easyGui = new SimpleSwing();
myFrame = new JFrame("SimpleSwing");
myFrame.addWindowListener(new WindowAdapter() {
public void windowClosing(WindowEvent e) {
System.exit(0);
}
  });
myFrame.getContentPane().add(myPanel);
myFrame.pack();
myFrame.setVisible(true);
    }
  }



