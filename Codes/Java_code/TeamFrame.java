 import java.util.Scanner;
 import java.io.File;
 import java.io.IOException;
 import javax.swing.JFrame;
 import javax.swing.JLabel;
 import java.awt.GridLayout;
 
 public class TeamFrame extends JFrame {
 	public TeamFrame() throws IOException {
 	BaseBallPlayer player;
 	Scanner myScanner = new Scanner(new File ("Hankees.txt"));
 	
 	for ( int num =1; num <= 9; num++) {
 		player = new BaseBallPlayer(myScanner.nextLine(), myScanner.nextDouble());
 		myScanner.nextLine();
 		addPlayerInfo(player);
 	}
 	
 	setTitle("The Hankees ");
 	setLayout(new GridLayout ( 9,2 ));
 	setDefaultCloseOperation( EXIT_ON_CLOSE);
 	pack();
 	setVisible(true);
 	}
 	
 	void addPlayerInfo(BaseBallPlayer player) {
 		add(new JLabel(player.getName()));
 		add(new JLabel (player.getAdverageString()));
 		}
 }
