/*
 * Wmvc Controller - implements a general purpose Controller using the command 
 * using the command pattern for Wmvc Framework
 *(c) 2011, Bilal Green
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class WmvcController implements ActionListener,ItemListener
 {
    protected JComponent myComponent;
    private WmvcExecutor wmvcExecutor; // The Executor object

// This constructor is used by the sub objects
public WmvcController(JComponent comp, String tip, WmvcExecutor wExec)
 {
 myComponent = comp;
 wmvcExecutor = wExec;
 if (tip!= null)
	myComponent.setToolTipText(tip);
 }

public WmvcExecutor getWmvcExecutor()
  { return wmvcExecutor; }

// implement the ActionListener
public void actionPerformed(ActionEvent event)
 {
    if(wmvcExecutor != null)
	wmvcExecutor.execute(event);
 }
// implement the itemListener
public void itemStateChanged(ItemEvent event)
{
    if(wmvcExecutor != null)
	wmvcExecutor.execute(event);

}








}
