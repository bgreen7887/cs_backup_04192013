/* 
* Wmvc Model - An MVC model superclass for the Wmvc Framework
* Copyright (c) 2011, Bilal Muhtadi Green
*/

import java.util.*;

public class WmvcModel extends Observable
 {

 // small class, just provides implementation of Observer pattern using MVC naming //

 public WmvcModel()
 {
    super();
 }

 public void addView(WmvcView view)
 {
    addObserver((Observer) view);
 }

 public void deleteView(WmvcView view)
 {
    deleteObserver((Observer) view);
 }

  public void notifyViews()
 {
    setChanged();
    notifyObservers();
 }



 }
