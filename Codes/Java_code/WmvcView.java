/*
 * WmvcView- a MVC view superclass for the Wmvc Framework
 *
 */

import java.util.*;

 public class WmvcView implements Observer
 {
 // implements the single Observer class
 public void update(Observable observed, Object value)
 {
    updateView();
 }
 
 public void updateView()
 {
    // no -op by default
 }


 }
