# notice the structure of the code and indentation 
# notice how code for cchecking each win is damn near exactly the same... copy/paste is your friend
def checkwin(player):
  #first loop through rows and colums !!!
  for c in range(0,3):
    #check  horizontal line
    if board[c][0] == player and board[c][1] == player and board[c][2] == player: # each statement check for a win
      	print "*********\n\n%s wins\n\n*********" % player
     	playerwin = True
return playerwin

    		#check vertical line
elif board[0][c] == player and board[1][c] == player and board[2][c] == player:
print "*********\n\n%s wins\n\n*********" % player
playerwin = True
return playerwin

   				 #check  diagonal win (left to right)
   				 elif board[0][0] == player and board[1][1] == player and board[2][2] == player:
    					  print "*********\n\n%s wins\n\n*********" % player
      					  playerwin = True
      					  return playerwin

    						#check diagonal win (right to left)
   						 elif board[0][2] == player and board[1][1] == player and board[2][0] == player:
      							print "*********\n\n%s wins\n\n*********" % player
      							playerwin = True
     							 return playerwin
  								else:
    									playerwin = False
    									return playerwin

#code for who's turn
def playerturn(player):
  print "%s's turn" % player
  turn = 1
  while(turn):
    print "Select column [1-3]: ",
    col = int(raw_input()) - 1
    print "Select row [1-3]: ",
    row = int(raw_input()) - 1
    if board[row][col] == 'X' or board[row][col] == 'O':
      print "Already taken!"
       else:
      	board[row][col] = player
      	turn = 0

def printboard():
  print board[0]
  print board[1]
  print board[2]

#make an empty board
board = [['-','-','-'],['-','-','-'],['-','-','-']] # simple board
player1 = 'X'
player2 = 'O'
win = False
turns = 0

#game loop
#print empty board to start
printboard()
while(win == False):
  	playerturn(player1)  # while game is still going, keep going back and fourth bwtween players
  	turns += 1
  	printboard()
  	if checkwin(player1) == True: break  # each time checking after a move is anyone won, if reach nine board full and tie.
  		if turns == 9:
    			print "This game is a draw!"
   			 break

  playerturn(player2)
  turns += 1
  printboard()
  checkwin(player2)
  if checkwin(player2) == True: break
