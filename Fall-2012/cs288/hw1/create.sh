#!/bin/bash
echo "              ########################       "
echo "              ######             #####       "
echo "                DIR. TREE CREATION           "
echo "                By Bilal Green:              "
echo "              #####              #####       "
echo "              #######################        "
declare -a open
# Breadth $ Depth are set by calling with args... ./create $2 $3 
d=$[ ($RANDOM % 3 ) + 1 ]     
b=$[ ($RANDOM % 5 ) + 1 ]
echo "Depth has been randomly chosen to be: $d"
echo "Breadth has been randomly chosen to be: $b"     
file=$1
count=1;


#echo mkdir $file
# Open is array ofy 1 element(the initial folder)
open=($file)

   # Depth array                    
  for((i=0;i < $d;i++));do   
     # count=`expr $count + 1`
      echo " "
      echo "Depth#$count"
      count=`expr $count + 1`
      new_open=()

      # Array takes care of every directory within subdirectory.
      for x in ${open[@]};do        

	  # Breadth array 
	  for((j=0;j < $b;j++));do     
               echo mkdir -p "$PWD/$x/$j"     
	      # tmp is set to be whatever mkdir produces(it copies mkdir)
              tmp=(${tmp[@]} $x/$j)
	  done
	  # new open copies everything that tmp has. tmp has everything mkdir has
	  new_open=(${tmp[@]})         
	  # closed array isn't actually utilized but good technique
	  # it remembers previous folders we've already looked at.
	  closed=(${closed[@]} $x)	 
      done
      open=${new_open[@]}
done



