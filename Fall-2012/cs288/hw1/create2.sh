#! /bin/bash

    declare -a lst
    declare -a open
    mkdir $3
    open=(Tree)
    for ((i=0; i<$1; i++));
    do

#	echo "i is now $i"
	declare -a new_open
	declare -a closed
	
	for x in ${open[@]};
	do

#	    echo "open is now ${open[@]}"
	    declare -a temp
	    for ((c=0; c<$2; c++));
	    do  
		 mkdir -p "$PWD/$x/$c"
		temp=(${temp[@]} $x/$c) 
#		echo "tmp is ${temp[@]}"
	    done
	new_open=(${temp[@]})
#	echo "new open is ${new_open[@]}"
	closed=(${closed[@]} $x)
	done
#	echo "Depth is $i"
        open=${new_open[@]}
#	echo "open2 is now ${open[@]}"
#	echo " "
    done
