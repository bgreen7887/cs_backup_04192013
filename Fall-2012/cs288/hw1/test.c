#include <stdio.h>

void print_args();
void print_details();
void print_strs();

int main(int argc, char **argv) {
  print_details(argc,argv);
  return 0;
}

void print_details(int argc, char **argv) {
  int i=0,n;

  printf("argc: addr=%x val=%x\n",&argc,argc);
  printf("argv: addr=%x val=%x\n\n",&argv,argv);
  while (*argv) {
    printf("arg%d: addr=%x val=%x: str addr=%x val hex=%x char=%c str=%s\n",
	   i++,argv,*argv,&(**argv),**argv,**argv,*argv);
    argv++;
  }
}
