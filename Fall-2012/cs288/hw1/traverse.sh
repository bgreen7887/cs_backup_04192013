#!/bin/bash

traverse() {
	# holds the root folder of directory.
	local d=$1
      	# save all directories in arry lst
	lst=`ls $d`
       	# change to tree directory
	cd "$d"	
	# comfirm element is a directory
	for x in ${lst[@]};
	do
	# if so echo it and traverse
	    if [ -d "$x" ]; then
		
		echo "Traversing ..."
	      	echo "$PWD/$x"
		traverse $x
        	fi
	       cd ..
	done
}
traverse $1
