#!/bin/sh
grep views index.html | grep -v span | cut -d' ' -f5 | sort -nr | tr -d "," > views.txt 

grep feed-item-owner index.html | grep -v  '<div' | while read x; do user=`expr "$x" : ".*user\/\(.*\)?.*"`; echo $user; done > user.txt

grep -A 3 -i data-sessionlink index.html | grep -v data-sessionlink | grep -v '</span' | grep -v '</div' | grep -v 'uploaded a video' | grep -v '>' | grep -v '^-' | awk 'NF' > title.txt

paste views.txt user.txt title.txt > all.txt

echo "Views:		User:				Title:"
cat all.txt
