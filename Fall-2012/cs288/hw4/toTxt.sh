#!/bin/bash

# save 1440 .html files inside lst variable to loop through.

lst=`ls html_files`

# create folder and save tables inside that folder
# ./html_files/$x

func() {
my_date=$(date +"%Y-%m-%d-%H-%M")

for x in ${lst[@]}; do

# Takes away commas and get number of views
    grep -n views ./html_files/$x | grep -v span | cut -d" " -f5 | sed s/,//g | sed '/^$/d'  > num_views.txt 

# Extracts users and sends to usr_names.txt
grep feed-item-owner ./html_files/$x | grep -v channel |  grep -v '<div' | while read x; do user=`expr "#$x" : ".*\/user/\(.*\)?feature.*\/"` ;echo $user; done > usr_name.txt;

# Extracts titles 
grep -A 3 -i data-sessionlink ./html_files/$x| grep -v data-sessionlink | grep -v '</span' | grep -v '</ydiv' | grep -v 'uploaded a video' | grep -v '>' | grep -v '^-' | awk 'NF' | sed '/^\--/d' > title.txt

paste num_views.txt usr_name.txt title.txt > $"index-$my_date.txt"

echo "#views:		usr_name:				title:"
cat all.txt
 
            
done
}
func
