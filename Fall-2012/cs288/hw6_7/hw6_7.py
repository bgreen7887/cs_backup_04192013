# Bilal Green 
import libxml2
import sys
import os
import commands
import re
import sys

import MySQLdb

from xml.dom.minidom import parse, parseString

# for converting dict to xml 
from cStringIO import StringIO
from xml.parsers import expat

def get_elms_for_atr_val(tag,atr,val):
   lst=[]
   elms = dom.getElementsByTagName(tag)
   # ............

   return lst

# get all text recursively to the bottom
def get_text(e):
   lst=[]
   # ............
   return lst

# replace whitespace chars
def replace_white_space(str):
   p = re.compile(r'\s+')
   new = p.sub(' ',str)   # a lot of \n\t\t\t\t\t\t
   return new.strip()

# replace but these chars including ':'
def replace_non_alpha_numeric(s):
   p = re.compile(r'[^a-zA-Z0-9:-]+')
   #   p = re.compile(r'\W+') # replace whitespace chars
   new = p.sub(' ',s)
   return new.strip()

# convert to xml

def convert_dict_to_xml(dic):
   s = StringIO()
   # ............
   xml = s.getvalue()
   return xml

def list_to_xml(name, l, stream,list_item_name):
   # ............
    return
def dict_to_xml(d, root_node_name, stream):
   # ............
    return

# convert to xhtml
# use: java -jar tagsoup-1.2.jar --files html_file
def html_to_xml(fn):
    os.system('java -jar tagsoup-1.2.1.jar --files ' + fn )
    fn = fn.replace('.html','')
    xhtml_file = fn + ".xhtml"
    return xhtml_file

# use the following keys: symbol, name, last, change, percent, volume
# watch out for change in mysql
def convert_lst_to_dict(l):
   d={}
   # ............
   return d

def extract_values(dm):
   lst = []
   l = get_elms_for_atr_val('tr','class','mwNormalLight')
   # ............
   #    get_text(e)
   # ............
   return lst

# mysql> describe most_active;
def insert_to_db(l,tbl):
   # ............

   return

# mysql> select symbol,name from most_active where name regexp '^c.m';
def select_from_db(cursor,tbl):
    # ............

   return 

# show databases;
# show tables;
def main():
   html_fn = sys.argv[1]
#   fn = html_fn.replace('.html','')
   xhtml_fn = html_to_xml(html_fn)
   
   print xhtml_fn
   global dom
   dom = parse(xhtml_fn)
   print dom
 
   lst = extract_values(dom)

   #dic = convert_lst_to_dict(lst)

   #xml = convert_dict_to_xml(dic)
   
  # xfname = fn + '.most_active.xml'
  # xml_fp = open(xfname,'w+')
  # xml_fp.write(xml)
  # xml_fp.close()
  # print 'xml file \'%s\' written' % xfname
   # ^^^^^^^^^^^^^^^^ up to here HW6 ^^^^^^^^^^^^^^^^^^

   # from here HW7
   # make sure your mysql server is up and running
  # cursor = insert_to_db(lst,fn) # fn = table name for mysql
  # l = select_from_db(cursor,fn)

   # make sure your Apache web server is up and running
   # you will need a seperate php file

   return #xml
# end of main()

if __name__ == "__main__":
    main()

# end of hw6-7.py
