#! /usr/bin/env python
# CS288 Homework 6-7
# Submitted by: Sebastian Serrano
# See readme textfile

import libxml2
import sys
import os
import commands
import re
import sys

import MySQLdb
import shlex
import subprocess
import glob

import xml.etree.ElementTree as etree
from xml.dom.minidom import parse, parseString, Document, Node

# for converting dict to xml 
from cStringIO import StringIO
from xml.parsers import expat

def extract_values(dm):
   	lst = []
   	temp = get_elms_for_atr_val('tr','class','lft')
	for e in temp:
		l = get_text(e)
		lst.append(l)
	return lst

def convert_dict_to_xml(dic):
   	l = ['Symbol','Name','Last','Change','Percent','Volume']
	doc = Document()
	tmp = doc.createElement("root")
	doc.appendChild(tmp)
	for x in range(len(dic)):
		tmp = doc.createElement('tr')
		doc.childNodes[0].appendChild(tmp)
		for i in range(len(l)):
			tmp = doc.createElement(l[i])
			text = doc.createTextNode(dic[x][l[i]])
			tmp.appendChild(text)
			doc.childNodes[0].childNodes[x].appendChild(tmp) 	
	return doc
#Changed my previous get_text to proffessor's way of doing it
def get_text(e):
   	lst=[]
	if e.nodeType in (3,4):
		if e.data != ' ':
			return [e.data]
	else:
		l = e.childNodes
		for x in l:
			lst = lst + get_text(x) 
	return lst

def get_elms_for_atr_val(tag,atr,val):
   lst=[]
   elms = dom.getElementsByTagName(tag)
   #print elms   
   for node in elms:
       if node.childNodes[0].getAttribute(atr)==val: 
          lst.append(node) 
   return lst

def replace_white_space(str):
   p = re.compile(r'\s+')
   new = p.sub(' ',str)   # a lot of \n\t\t\t\t\t\t
   return new.strip()

# replace but these chars including ':'
def replace_non_alpha_numeric(s):
   p = re.compile(r'[^a-zA-Z0-9:-]+')
   #p = re.compile(r'\W+') # replace whitespace chars
   new = p.sub(' ',s)
   return new.strip()

def convert_dict_to_xml(dic):
   s = StringIO()
   xml = s.getvalue()
   return xml

def list_to_xml(name, l, stream,list_item_name):
   # ............
   return 
def dict_to_xml(d,root_node_name,stream):
   # ............
   return
def html_to_xml(fn):
   #cdir = os.getcwd()  
   #os.chdir(cdir) 
   # cmd = os.system(' java -jar tagsoup-1.2.1.jar --files ' + fn)
   #xhtml_file = subprocess.Popen(['java',' -jar',' tagsoup-1.2.1.jar',' --files', fn],
   #       stderr=subprocess.STDOUT,stdout=subprocess.PIPE)
   #xhtml_file = os.getcwd() + '/' + fn + '.xhtml'
   #tp = glob.glob('*.xhtml') 
   #fn = fn.replace('.xhtml','')
   #EASIER
   os.system(' java -jar tagsoup-1.2.1.jar --files ' + fn)
   fn = fn.replace('.html','') 
   xhtml_file = fn + ".xhtml"
   #path = os.path.abspath(tp[0]) #<--tmp workaround fix later
   #print path
   #xhtml_file = path
   return xhtml_file
def simplifyList(l):
    lst = []
    for x in l:
        for y in x: 
            lst.append(y)
    return lst

#Create a dictionary of list based on the 6 keys
def convert_lst_to_dict(l):
  d={}
  key = ['Symbol','Name','Last','Change','Percent','Volume']  
  iterator = 0
  count = 0  
 
  d = {'Symbol' : [],'Name' : [],'Last' : [],'Change' : [] ,'Percent' : [],'Volume' : []}
  for element in l:
       #iterator += 1
       if iterator == 0:
          d['Symbol'].append(element)
       elif iterator == 1:
          d['Name'].append(element)
       elif iterator == 2:
          d['Last'].append(element)
       elif iterator == 3:
          d['Change'].append(element)  
       elif iterator == 4:
          d['Percent'].append(element)      
       elif iterator == 5:
          d['Volume'].append(element)   
       elif iterator == 6:
          d['Symbol'].append(element) 
          iterator = 0 
       iterator +=  1
       count += 1
  return d
# mysql> describe most_active;
def insert_to_db(l,tbl):
   # mysql> select symbol,name from most_active where name regexp '^c.m';
   return d
def select_from_db(cursor,tbl):
   # show databases;
   # show tables;
   return d

def main():
   html_fn = sys.argv[1]
   fn = html_fn.replace('.html','')
   xhtml_fn = html_to_xml(html_fn)
   print xhtml_fn 
   global dom
   dom = parse(xhtml_fn) #<-- DOM Verified
   lst = extract_values(dom)
   newlst = simplifyList(lst)
   dic = convert_lst_to_dict(newlst)
   
   xml = convert_dict_to_xml(dic)
   xfname = fn + 'most_active.xml'
   xml_fp = open(xfname,'w+')
   xml_fp.write(xml)
   xml_fp.close()
   print 'xml file \'%s\' written' % xfname
   # ^^^^^^^^^^^^^^^^ up to here HW6 ^^^^^^^^^^^^^^^^^^

   # from here HW7
   # make sure your mysql server is up and running
   #cursor = insert_to_db(lst,fn) # fn = table name for mysql
   #l = select_from_db(cursor,fn)

   # make sure your Apache web server is up and running
   # you will need a seperate php file

   #return xml
# end of main()
if __name__ == "__main__":
   main()
