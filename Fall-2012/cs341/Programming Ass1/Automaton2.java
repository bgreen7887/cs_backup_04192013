
/*
 * Bilal Green
 *
 */

import java.util.Scanner;
public class Automaton2 {
    public static String url = "";
    public static String alpha = "abcdefghijklmnopqrstuvwxyz";
    public static int i;

    public static void main(String[] args) {	
	Scanner my_scan = new Scanner(System.in);
	String user_input1 = "";

	while (!user_input1.equals("n")) {
	    i=0;
	    System.out.println("Are you going to enter a string (y)es or (n)o ?");
	    user_input1 = my_scan.next();
	      if (user_input1.equals("y")) {
		  System.out.println("Enter String now");
		  url =  my_scan.next();
		  System.out.println("You wanted '" + url + "' . Right?");
		  try {
		  q0(url);
		  }
		  catch (StringIndexOutOfBoundsException e) {}
	      } else if (user_input1.equals("n")) { 
		  break;
	      }

	}	 
		  
    } // End of Main()    
    public static Boolean q0(String url) {
    System.out.println("*START-STATE* {q0}");
	if (i < url.length()) {
	     System.out.println(":"+ url.charAt(i));
	    if (url.charAt(i) == 'w') {
			i++;
		return q1();
	    }
	    else if (alpha.contains(url.charAt(i)+"") && url.charAt(i) != 'w') {
	    	 i++;
	     return q12();
	    }
	System.out.println("REJECTED by {q0}");

	}
	return false;
   } // End of q0()

    public static Boolean q1() {
    System.out.println("In state {q1}");
	if (i < url.length()) {
	     System.out.println(":" + url.charAt(i));
	    if (url.charAt(i) == 'w') {
		i++;
		return q2();
	    }
	    else if (alpha.contains(url.charAt(i)+"") && url.charAt(i) != 'w') {
	     i++;
	     return q12();
	    }
	}
	System.out.println("REJECTED by {q1}");
	return false;
        } // End of q1()

    public static Boolean q2() {
    System.out.println("In state {q2}" );
	if (i < url.length()) {
	     System.out.println(":" + url.charAt(i));
	    if (url.charAt(i) == 'w') {
		i++;
		return q3();
	    }
	    else if (alpha.contains(url.charAt(i)+"") && (url.charAt(i) != 'w')){
	    i++;
	   return q12();
	    }
	    else if (url.charAt(i) == '.') {
		i++;
	    return q13();
            }
	}
	System.out.println("REJECTED by {q2}");
	return false;
	    } // End of q2()

    public static Boolean q3() {
	System.out.println("In state {q3}");
	if (i < url.length()) {
	     System.out.println(":" + url.charAt(i));
	    if (url.charAt(i) == '.') {
	    i++;
	    return q4();
	} 
	else if (alpha.contains(url.charAt(i)+"")) {
		i++;
		return q12();
	    }
    }
	System.out.println("REJECTED by {q3}");
	return false;
    } // End of q3()

    public static Boolean q4() {
	System.out.println("In state {q4}");
	if (i < url.length()) {
	     System.out.println(":" + url.charAt(i));
	    if (url.charAt(i) == 'e') {
	    i++;
	    return q5();
	} 
	    else if (alpha.contains(url.charAt(i)+"") && (url.charAt(i) != 'e')) {
		i++;
		return q12();
		}
	}
	System.out.println("REJECTED by {q4}");
	return false;
    } // End of q4()
  
     public static Boolean q5() {
	System.out.println("In state {q5}");
	if (i < url.length()) {
	     System.out.println(":" + url.charAt(i));
	    if(url.charAt(i) == 'd') {
	    i++;
	    return q6();
	} 
	else if (alpha.contains(url.charAt(i)+"")) {
		i++;
		return q14();
	    }
	}
	System.out.println("REJECTED by {q5}");
       	return false;
    }

    public static Boolean q6() {
    System.out.println("In state {q6}");
	if (i < url.length()) {
	     System.out.println(":" + url.charAt(i));
	    if (url.charAt(i) == '.') {
	    i++;
	    return q4();
	} 
	else if (url.charAt(i) == 'u') {
		i++;
		return q7();
	    }
	else if (alpha.contains((url.charAt(i) +"")) && url.charAt(i) != 'u') {
	    i++;
		return q12();
	}
	}
	System.out.println("REJECTED by {q6}");
	return false;
     }
  
 
    public static Boolean q7() {
    System.out.println("In state {q7}");
	if (i < url.length()) {
	     System.out.println(":" + url.charAt(i));
	    if (alpha.contains(url.charAt(i)+"")) {
		i++;
		return q12();
	    }
	    else if ( url.charAt(i) == '.') {
		i++;
		return q8();
	    }
	}
	System.out.println("ACCEPTED by {q7}");
	return true;
    }

    public static Boolean q8() {
    System.out.println("In state {q8}");
	if (i < url.length()) {
	     System.out.println(":" + url.charAt(i));
	    if ( url.charAt(i) == 'e') {
		i++;
		return q9();
	    }
	    else if (alpha.contains( url.charAt(i)+"") && url.charAt(i) != 'e') {
		i++;
		return q14();
	    }
	}
	System.out.println("REJECTED by {q8}");
	return false;
    }

    public static Boolean q9() {
    System.out.println("In state {q9}");
	if (i < url.length()) {
	     System.out.println(":" + url.charAt(i));
	    if ( url.charAt(i) == 'd') {
		i++;
		return q10();
	    }
	    else if (alpha.contains( url.charAt(i)+"") && url.charAt(i) != 'd') {
		i++;
		return q14();
	    }
	}
	System.out.println("REJECTED by {q9}");
	return false;
    }

    public static Boolean q10() {
    System.out.println("In state {q10}");
	if (i < url.length()) {
	     System.out.println(":" + url.charAt(i));
	    if ( url.charAt(i) == 'u') {
		i++;
		return q11();
	    }
	    else if (alpha.contains( url.charAt(i) +"") && url.charAt(i) != 'u') {
		i++;
		return q14();
	    }
	}
	System.out.println("REJECTED by {q10}");
	return false;
 }

     public static Boolean q11() {
     System.out.println("In state {q11}");
	if (i < url.length()) {
	     System.out.println(":" + url.charAt(i));
	    if (alpha.contains(url.charAt(i)+"")) {
		i++;
		return q14();
	    }
	else if ( url.charAt(i) == '.') {
		i++;
		return q14();
	    }
	}
	System.out.println("ACCEPTED by {q11}");
	return true;
    }

    public static Boolean q12() {
    System.out.println("In state {q12}");
	if (i < url.length()) {
	    System.out.println(":" + url.charAt(i));	    
	    if (alpha.contains(url.charAt(i)+"")) {
		i++;
		return q12();
	    } 
	    else if (url.charAt(i) == '.') {
		i++;
		return q13();
	    }
	}
	System.out.println("REJECTED by {q12}");
	return false;
 }

    public static Boolean q13() {
     System.out.println("In state {q13}");
    if (i < url.length()) {
	 System.out.println(":" + url.charAt(i));
	if (url.charAt(i) == 'e') {
	    i++;
	   return q9();
	}
    else if ((alpha.contains(url.charAt(i)+"") && url.charAt(i) != 'e')) {
	i++;
	return q12();
    }
    
}
	System.out.println("REJECTED by {q13}");
    return false;
}

    public static Boolean q14() {
    System.out.println("*TRAP STATE* {q14}");
    if ( i < url.length()) {
	 System.out.println(":" + url.charAt(i));
	if (alpha.contains(url.charAt(i)+"")) {
	    i++;
	    return q14();
	}
    }
    System.out.println("REJECTED by {q14}");
    return false;
    }
} // End of Automaton class

