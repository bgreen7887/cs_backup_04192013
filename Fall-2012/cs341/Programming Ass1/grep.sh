#!/bin/bash

#Download youtube index
wget -O index.html http://www.youtube.com

#Find lines with a diget followed by "views" and write to text file
grep -e "[0-9] views" index.html | grep -v ltr > views.txt

#Remove all ',' and 'views' and echo
cat views.txt | while read line; do
	tmp="${line//views/}"
	tmp="${tmp//,/}"
	echo $tmp
done

#Find lines with 'video-list-item' within them and write to text file
grep -e "video-list-item" index.html > names.txt

#Echo each line
cat names.txt | while read line; do
	for word in `cat names.txt`; do
	#	if [ "$word" == "alt=" ]; then
	#		echo "$word"
	#	fi
		echo -e "$word\n"	
	done
	echo -e "$line\n"
done
