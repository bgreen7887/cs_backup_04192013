//package com.njit.Bilal;

/*
 * Bilal Green
 *
 */

import java.util.Scanner;
import java.util.Stack;

public class Pda {
	public static int i;
	public static String alpha = "abcdefghijklmnopqrstuvwxyz";
	public static String operat = "+-*/";
	public static String number = "0123456789";
	public static String leftparen = "(";
	public static String rightparen = ")";
	public static String dollar = "$";
	public static String expression = "";
	public static Stack  myStack = new Stack ();
	public static void main(String[] args) {
	Scanner my_scan = new Scanner(System.in);
       	String user_input1 = "";
	// while loop governs the game iterations until user enter 'n' game will continue
	while (!user_input1.equals("n")) {
	    // clear stack after each round 
		myStack.clear();
		i = 0;
		System.out.println("Are you going to enter a string (y)es or (n)o ?");
		user_input1 = my_scan.next();
		if (user_input1.equals("y")) {
			System.out.println("Enter String now");
			expression = my_scan.next();
			System.out.println("Expression entered was " + expression
						+ '\n');
			try {
			    // have to return a true from last method & stack should be empty
			    if (q0() && myStack.empty()) {
				System.out.println("Expression Accepted :-) \n\n");
			    } else {
					System.out.println("Expression Rejected :-( \n\n");
			    }
				} catch (StringIndexOutOfBoundsException e) {
				}
			} else if (user_input1.equals("n")) {
				break;
		}
	}
	} // end of main()
        
    // start state ensures first letter is $ if not Crash
    // each method corr. to transitions in pda 
	public static Boolean q0() {
		System.out.println("Start State = {q0}");
		if (i < expression.length()) {
			System.out.println("I read a: " + expression.charAt(i));
			if (dollar.contains(expression.charAt(i) + "")) {
				myStack.push(expression.charAt(i));
				System.out.println(expression.charAt(i)
						+ " Pushed onto the stack");
				i++;
				return q1();
			} else {
				System.out.println("Crashed In " + "{q0}");
			}
		}
		return false;
	}
    
	public static Boolean q1() {
		System.out.println("\nState = {q1}");
		if (i < expression.length()) {
			System.out.println("I read a: " + expression.charAt(i));
			if (alpha.contains(expression.charAt(i) + "")) {
				System.out.println("Nothing pushed or popped from stack!");
				i++;
				return q2();
			} else if (leftparen.contains(expression.charAt(i) + "")) {
				System.out.println("Nothing popped from the stack!");
				myStack.push(expression.charAt(i));
				System.out.println(expression.charAt(i)
						+ " Pushed onto the stack");
				i++;
				return q1();
			} else {
				System.out.println("Crashed In " + "{q1}");
			}
		}
		return false;
	}

	public static Boolean q2() {
		System.out.println("\nState = {q2}");
		if (i < expression.length()) {
			System.out.println("I read a: " + expression.charAt(i));
			// reading a # or char doesn't make a diff. in this state!
			if (alpha.contains(expression.charAt(i) + "")
					|| (number.contains(expression.charAt(i) + ""))) {
				System.out.println("Nothing pushed or popped from stack!");
				i++;
				return q3();
			} else {
				System.out.println("Crashed In " + "{q2}");
			}
		}
		return false;
	}

	public static Boolean q3() {
		System.out.println("\nState = {q3}");
		if (i < expression.length()) {
			System.out.println("I read a: " + expression.charAt(i));
			// check paren. make sure opposite paren. is on the stack already
			if (rightparen.contains(expression.charAt(i) + "")) {
				if(myStack.peek().equals('(')) {
					myStack.pop();
					System.out.println("Nothing pushed onto the stack!");
					System.out.println(expression.charAt(i)
						+ " popped from the stack");
						i++;
						return q3();
				}
			}
			else if (dollar.contains(expression.charAt(i) + "")) {
					myStack.pop();
					System.out.println("Nothing pushed onto the stack!");
					System.out.println(expression.charAt(i)
						+ " popped from the stack");
				i++;
				return q4();
			}
			else if (operat.contains(expression.charAt(i) + "")) {
				System.out.println("Nothing pushed or popped from stack!");
				i++;
				return q1();
			}
		}
				return false;
	}
    // accept state 
	public static Boolean q4() {
		return true;
	}
	
} // end of class Pda.java

