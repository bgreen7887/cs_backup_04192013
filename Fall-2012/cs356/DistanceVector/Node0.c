#include <stdio.h>
#include <stdlib.h>
#define INFINITY 9999

 struct rtpkt {
  int sourceid;      
  int destid;         
  int mincost[4];    
  };

extern int TRACE;
extern int YES;
extern int NO;
extern float clocktime;

int l_cost[4];
int s_path[4];

struct distance_table 
{
  int costs[4][4];
} dt0;

void rtinit0() 
{
	int i,j;
	printf("At time t=%.3f, rtinit0() was called. \n",clocktime);
	
	l_cost[0] = 0;
	l_cost[1] = 1;
	l_cost[2] = 3;
	l_cost[3] = 7;

	for (i=0;i<4;i++) 
		for (j=0;j<4;j++) {
			if (i==0) 
				dt0.costs[i][j] = l_cost[j];
			else
				dt0.costs[i][j] = INFINITY;
	}	
	for (i=0; i<4; i++) 
		s_path[i]=l_cost[i];
		printdt0(&dt0);
		send_packet0();
}

void rtupdate0(rcvdpkt)
  struct rtpkt *rcvdpkt;
{
int i, j;
	j=rcvdpkt->sourceid; 
	printf("At time t=%.3f, rtupdate0() called, and node 0 receives a packet from node %d\n",
	clocktime, j);
	
	for ( i= 0; i<4; i++) {
		dt0.costs[j][i] = l_cost[j] + rcvdpkt->mincost[i];
		if (dt0.costs[j][i] > INFINITY)
			dt0.costs[j][i]=INFINITY;
	}
	printdt0(&dt0);				
	if (update_me0() == 1)	send_packet0();	
}
 printdt0(dtptr) 
  struct distance_table *dtptr;
  
{
  printf("                        \n");
  printf("   D0 |    1     2    3 \n");
  printf("  ----|-----------------\n");
  printf("     1|  %3d   %3d   %3d\n",dtptr->costs[0][1],
	dtptr->costs[0][2],dtptr->costs[0][3]);
  printf("     2|  %3d   %3d   %3d\n",dtptr->costs[1][1],
	 dtptr->costs[1][2],dtptr->costs[1][3]);
  printf("     3|  %3d   %3d   %3d\n",dtptr->costs[2][1],
	 dtptr->costs[2][2],dtptr->costs[2][3]);
}

linkhandler0(linkid, newcost)   
  int linkid, newcost; {}
 
int update_me0()
{
	int i, j;
	int tmp[4];
	int flag =0;
	
	for (i=0; i<4; i++) {	
		tmp[i] = dt0.costs[i][0];
		for (j=1; j<4; j++) {
			if (tmp[i] > dt0.costs[i][j]) tmp[i] = dt0.costs[i][j];
		}
		if (tmp[i] < s_path[i] ) {
			s_path[i] = tmp[i];
			flag =1;
		}
	}
	return flag;
}

send_packet0()
{
	int i;
	struct rtpkt packet;

	/* package packet */
	packet.sourceid = 0;
	for (i=0; i<4; i++) 
		packet.mincost[i] = s_path[i];

	/*send the packet to the correct neighbor*/
	packet.destid=1;
	tolayer2(packet);
	printf("At time t=%.3f, node 0 sends packet to node 1 with:%d %d %d\n",
		clocktime,packet.mincost[1], packet.mincost[2],packet.mincost[3]);

	packet.destid=2;
	tolayer2(packet);
	printf("At time t=%.3f, node 0 sends packet to node 2 with:%d %d %d\n",
		clocktime, packet.mincost[1], packet.mincost[2],packet.mincost[3]);

	packet.destid=3;
	tolayer2(packet);
	printf("At time t=%.3f, node 0 sends packet to node 3 with:%d %d %d\n",
		clocktime,packet.mincost[1], packet.mincost[2],packet.mincost[3]);

	printf("\n");

}



