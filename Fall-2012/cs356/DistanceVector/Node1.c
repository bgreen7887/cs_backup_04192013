#include <stdio.h>
#include <stdlib.h>
#define INFINITY 9999

 struct rtpkt {
  int sourceid;      
  int destid;         
  int mincost[4];   
  };

extern int TRACE;
extern int YES;
extern int NO;
extern float clocktime;

int l_cost1[4];
int s_path1[4];

struct distance_table 
{
  int costs[4][4];
} dt1;

void rtinit1() 
{
	int i,j;
	printf("At time t=%.3f, rtinit1() was called. \n",clocktime);
	l_cost1[1] = 0;
	l_cost1[0] = 1;
	l_cost1[2] = 1;
	l_cost1[3] = INFINITY;
	
	for (i=0;i<4;i++) 
		for (j=0;j<4;j++) {
			if (i==1) 
				dt1.costs[i][j] = l_cost1[j];
			else
				dt1.costs[i][j] = INFINITY;
	}
		for (i=0; i<4; i++) 
			s_path1[i]=l_cost1[i];
		send_packet1();
}

void rtupdate1(rcvdpkt)
  struct rtpkt *rcvdpkt;
{
int i, j;
	j=rcvdpkt->sourceid;
	printf("At time t=%.3f, rtupdate1() called, and node 1 receives a packet from node %d\n",
		clocktime, j);
		
	for ( i= 0; i<4; i++) {
		dt1.costs[j][i] = l_cost1[j] + rcvdpkt->mincost[i];
		if (dt1.costs[j][i] > INFINITY)
			dt1.costs[j][i]=INFINITY;
	}
	if (update_me1() == 1)	send_packet1();	
}

linkhandler1(linkid, newcost)   
  int linkid, newcost; {}
 
int update_me1()
{
	int i, j;
	int tmp[4];
	int flag =0;

	for (i=0; i<4; i++) {	
		tmp[i] = dt1.costs[i][0];
		for (j=0; j<4; j++) {
			if (tmp[i] > dt1.costs[i][j]) tmp[i] = dt1.costs[i][j];
		}

		if (tmp[i] < s_path1[i] ) {
			s_path1[i] = tmp[i];
			flag =1;
		}
	}
	return flag;
}

send_packet1()
{
	int i;
	struct rtpkt packet;

	/* package packet*/
	packet.sourceid = 1;
	for (i=0; i<4; i++) 
		packet.mincost[i] = s_path1[i];

	/*send the packet to the correct neighbor*/
	packet.destid=0;
	tolayer2(packet);
	printf("At time t=%.3f, node 1 sends packet to node 0 with: %d %d %d\n",
		clocktime, packet.mincost[0],packet.mincost[2],packet.mincost[3]);

	packet.destid=2;
	tolayer2(packet);
	printf("At time t=%.3f, node 1 sends packet to node 2 with: %d %d %d\n",
		clocktime, packet.mincost[0],packet.mincost[2],packet.mincost[3]);
	printf("\n");
}



