#include <stdio.h>
#include <stdlib.h>
#define INFINITY 9999

 struct rtpkt {
  int sourceid;       
  int destid;        
  int mincost[4];   
  };

extern int TRACE;
extern int YES;
extern int NO;
extern float clocktime;

int l_cost2[4];
int s_path2[4];

struct distance_table 
{
  int costs[4][4];
} dt2;

void rtinit2() 
{
	int i,j;
	printf("At time t=%.3f, rtinit2() was called. \n",clocktime);
	l_cost2[1] = 1;
	l_cost2[0] = 3;
	l_cost2[2] = 0;
	l_cost2[3] = 2;
	
	for (i=0;i<4;i++) 
		for (j=0;j<4;j++) {
			if (i==2) 
				dt2.costs[i][j] = l_cost2[j];
			else
				dt2.costs[i][j] = INFINITY;
	
		}
		for (i=0; i<4; i++) 
			s_path2[i]=l_cost2[i];
		sendpkt2();
}

void rtupdate2(rcvdpkt)
  struct rtpkt *rcvdpkt;
{
int i, j;
	j=rcvdpkt->sourceid;

	printf("At time t=%.3f, rtupdate2() called, and node 2 receives a packet from node %d\n",
		clocktime, j);
		
	for ( i= 0; i<4; i++) {
		dt2.costs[i][j] = l_cost2[j] + rcvdpkt->mincost[i];
		if (dt2.costs[i][j] > INFINITY)
			dt2.costs[i][j]=INFINITY;
	}

	if (update2() == 1)	sendpkt2();	
}

linkhandler2(linkid, newcost)   
  int linkid, newcost; {}
 
int update2()
{
	int i, j;
	int tmp[4];
	int flag =0;

	for (i=0; i<4; i++) {	
		tmp[i] = dt2.costs[i][0];
		for (j=1; j<4; j++) {
			if (tmp[i] > dt2.costs[i][j]) tmp[i] = dt2.costs[i][j];
		}

		if (tmp[i] != s_path2[i] ) {
			s_path2[i] = tmp[i];
			flag =1;
		}
	}
	return flag;
}

sendpkt2()
{
	int i;
	struct rtpkt packet;

	/*make the packet*/
	packet.sourceid = 2;
	for (i=0; i<4; i++) 
		packet.mincost[i] = s_path2[i];

	/*send the packet to the correct neighbor*/
	packet.destid=0;
	tolayer2(packet);
	printf("At time t=%.3f, node 2 sends packet to node 0 with: %d %d %d\n",
		clocktime, packet.mincost[0], packet.mincost[1],packet.mincost[3]);

	packet.destid=1;
	tolayer2(packet);
	printf("At time t=%.3f, node 2 sends packet to node 1 with: %d %d %d\n",
		clocktime, packet.mincost[0], packet.mincost[1],packet.mincost[3]);

	packet.destid=3;
	tolayer2(packet);
	printf("At time t=%.3f, node 2 sends packet to node 3 with: %d %d %d\n",
		clocktime, packet.mincost[0], packet.mincost[1],packet.mincost[3]);

	printf("\n");

}



