#include <stdio.h>
#include <stdlib.h>
#define INFINITY 9999

 struct rtpkt {
  int sourceid;       
  int destid;         
  int mincost[4];    
  };

extern int TRACE;
extern int YES;
extern int NO;
extern float clocktime;

int l_cost3[4];
int s_path3[4];

struct distance_table 
{
  int costs[4][4];
} dt3;

void rtinit3() 
{
	int i,j;
	printf("At time t=%.3f, rtinit3() was called to service. \n",clocktime);
	l_cost3[1] = INFINITY;
	l_cost3[0] = 7;
	l_cost3[2] = 2;
	l_cost3[3] = 0;
	
	for (i=0;i<4;i++) 
		for (j=0;j<4;j++) {
			if (i==3) 
				dt3.costs[i][j] = l_cost3[j];
			else
				dt3.costs[i][j] = INFINITY;
	
		}
		for (i=0; i<4; i++) 
			s_path3[i]=l_cost3[i];
		sendpkt3();
}

void rtupdate3(rcvdpkt)
  struct rtpkt *rcvdpkt;
{
int i, j;
	j=rcvdpkt->sourceid;
	printf("At time t=%.3f, rtupdate3() called, and node 3 receives a packet from node %d\n",
		clocktime, j);
		
	for ( i= 0; i<4; i++) {
		dt3.costs[j][i] = l_cost3[j] + rcvdpkt->mincost[i];
		if (dt3.costs[j][i] > INFINITY)
			dt3.costs[j][i]=INFINITY;
	}

	if (update3() == 1)	sendpkt3();	

}

linkhandler3(linkid, newcost)   
  int linkid, newcost; {}
 
int update3()
{
	int i, j;
	int tmp[4];
	int flag =0;

	for (i=0; i<4; i++) {	
		tmp[i] = dt3.costs[i][0];
		for (j=1; j<4; j++) {
			if (tmp[i] > dt3.costs[i][j]) tmp[i] = dt3.costs[i][j];
		}

		if (tmp[i] != s_path3[i] ) {

			s_path3[i] = tmp[i];
			flag =1;
		}
	}

	return flag;
}

sendpkt3()
{
	int i;
	struct rtpkt packet;

	/*make the packet*/
	packet.sourceid = 3;
	for (i=0; i<4; i++) 
		packet.mincost[i] = s_path3[i];

	/*send the packet to the correct neighbor*/
	packet.destid=0;
	tolayer2(packet);
	printf("At time t=%.3f, node 3 sends packet to node 0 with: %d %d %d\n",
		clocktime, packet.mincost[0], packet.mincost[1], packet.mincost[2] /*,packet.mincost[3]*/);

	packet.destid=2;
	tolayer2(packet);
	printf("At time t=%.3f, node 3 sends packet to node 2 with: %d %d %d\n",
		clocktime, packet.mincost[0], packet.mincost[1], packet.mincost[2] /*,packet.mincost[3]*/);
			printf("\n");

}



