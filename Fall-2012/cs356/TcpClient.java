/*
 Bilal Green
 Tcp Socket Programming
 TcpClient.java
 Sever waits for client to connect. Program executes by sending client message into socket to server.
 Sever recieves message & returns it to client Capitalized using java method toUpper().
*/




import java.io.*;
import java.net.*;

public class TcpClient {
    public static void main (String[] args) {
	// sentence object holds users original string
	// modified is the string the server returns
	String sentence;
	String modifiedSentence;
	
	// inFromUser stores stream of chars until user enters newline
	BufferedReader inFromUser = new BufferedReader ( new InputStreamReader(
									       System.in));
	try { 
        // create Socket hostname param must be changed
        Socket clientSocket = new Socket("128.235.234.184", 45654);
	// create 2 streams one processes output to socket
	// the other processes input from the server

	// create 2 stream objects that are attached to the socket
	DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
	BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
	sentence = inFromUser.readLine();
	outToServer.writeBytes(sentence + '\n' );
	modifiedSentence = inFromServer.readLine();
	System.out.println("From Server: " + modifiedSentence);
	clientSocket.close();
	}
	catch(UnknownHostException e) {
	    System.out.println("That host is no good.");
	    System.exit(1);
	}
       	catch(IOException e) {
        System.out.println("Couldn't get I/O for " + " the connection to: 128.235.234.184");
        System.exit(1);
	}
       
    }

}
