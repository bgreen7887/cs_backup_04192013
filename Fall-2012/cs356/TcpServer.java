/*
 Bilal Green
 Tcp Socket Programming
 TcpServer.java
 Sever waits for client to connect. Program executes by sending client message into socket to server.
 Sever recieves message & returns it to client Capitalized using java method toUpper().
*/



import java.io.*;
import java.net.*;

public class TcpServer {
    public static void main(String[] args) {
	String clientSentence = null;
	String capitalizedSentence = null;
	try {
	ServerSocket welcomeSocket = new ServerSocket(45654);
	
	while(true) {
	    Socket connectionSocket = welcomeSocket.accept();
	    BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
	    DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
	    clientSentence = inFromClient.readLine();
	    // after storing line read from client capitalize it and send back.
	    capitalizedSentence = clientSentence.toUpperCase() + '\n';
	    outToClient.writeBytes(capitalizedSentence);
	}
	}
	catch (java.io.IOException e) {
		System.exit(1);
    }
  }
}


