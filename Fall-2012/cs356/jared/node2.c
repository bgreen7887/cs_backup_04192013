#include <stdio.h>
#define INFINITY 999

 struct rtpkt {
  int sourceid;       /* id of sending router sending this pkt */
  int destid;         /* id of router to which pkt being sent 
                         (must be an immediate neighbor) */
  int mincost[4];    /* min cost to node 0 ... 3 */
  };

extern int TRACE;
extern int YES;
extern int NO;

struct distance_table 
{
  int costs[4][4];
} dt2;

struct rtpkt packet;

/* students to write the following two routines, and maybe some others */

void rtinit2() 
{

	packet.mincost[0] = 3;
	packet.mincost[1] = 1;
	packet.mincost[2] = 0;
	packet.mincost[3] = 2;

	
        printf("inside rinit2 function");
	
	int i;
	int j;

	//sets all distances to node i from node j to infinity
	for(i=0;i<4;i++)
	{
		for(j=0; j<4; j++)
		{
			dt2.costs[i][j]= 999;
		}
	}
	
        //sets the direct distances from node 0 to nodes 1,2,3 according 
        //to the lab
	dt2.costs[1][1]=1;
	dt2.costs[2][2]=3;
	dt2.costs[3][3]=7;

	//for each distance through node i from node j, check if minimum cost is        //smaller than infinity, if so set it to the minimum cost, if not keep i        t at infinity
	for(i=0;i<4;i++)
	{
		for(j=0; j<4; j++)
		{
			if(packet.mincost[i] > dt2.costs[i][j])
				packet.mincost[i] = dt2.costs[i][j];
		}
	}
        //set packet's sourceid to 2, since this code is for node 2
        //calling function tolayer2 in order for information to be transferred
        //from network layer to layer 2(data link layer)
	packet.sourceid = 2;
	packet.destid = 1;
	tolayer2(packet);  //simulate layer 2 with the given source&destination

struct rtpkt packet;
	packet.sourceid = 2;
	packet.destid = 3;
	tolayer2(packet);  //simulate data link layer
	printf("done with rtinit0\n");
}


void rtupdate2(rcvdpkt)
  struct rtpkt *rcvdpkt;
  
{
//setting all minimum costs to nodes 0,1,2,3 to infinity
        packet.mincost[0] = 999;
	packet.mincost[1] = 999;
	packet.mincost[2] = 999;
	packet.mincost[3] = 999;
	
	printf("inside rtupdate2\n");

	//prints out updates of packets sent to sourceid = node 0
	//with respect to time
	extern float clocktime; //from prog3.c
	printf("rtupdate2: %f\n sender: %d\n", clocktime, rcvdpkt->sourceid);
	//printdt0(&dt2);

	
	int temp[4];
	int change = 0;
	int i;
	int j;
	for(i = 0; i < 4; ++i)
	{
		//checks for updates to see if any cost should be changed
               // this is done by switching the value change from 0 to 1
		if(dt2.costs[i][rcvdpkt->sourceid] > rcvdpkt->mincost[i] +  dt2.costs[rcvdpkt->sourceid][rcvdpkt->sourceid])
		{
			dt2.costs[i][rcvdpkt->sourceid] = rcvdpkt->mincost[i]  + dt2.costs[rcvdpkt->sourceid][rcvdpkt->sourceid]; 
			change = 1;
		}
	}
	//if the change has actually happened, update the table in accordance with the change, i.e. change minimum cost from 
        //infinity to the distance vector cost according to the diagram
	if(change)
	{
		for(i=0;i<4;i++)
		{
			for(j=0; j<4; j++)
			{
				if(packet.mincost[i]  > dt2.costs[i][j]) 
					packet.mincost[i] = dt2.costs[i][j];
			}
		}

		printf("finished updating distance table\n");


		
		//calls function tolayer2 to simulate data link layer after table has been updated 
		packet.sourceid = 2;
		packet.destid = 1;
		tolayer2(packet);

		packet.sourceid = 2;
		packet.destid = 3;
		tolayer2(packet);

		packet.sourceid = 2;
		packet.destid = 0;
		tolayer2(packet);
		
	
	}
}










