#include <stdio.h>
#define INFINITY 999

 struct rtpkt {
  int sourceid;       /* id of sending router sending this pkt */
  int destid;         /* id of router to which pkt being sent
                         (must be an immediate neighbor) */
  int mincost[4];    /* min cost to node 0 ... 3 */
  };

extern int TRACE;
extern int YES;
extern int NO;

struct distance_table
{
  int costs[4][4];
} dt3;


struct rtpkt packet;
/* students to write the following two routines, and maybe some others */


/*rtinit3 is similar to rtinit0, 1 and 2.  That is, it's in charge of calculating the shortest distance vector from node 3 to nodes 0,1,2 */
void rtinit3()
{

	extern float clocktime;

	packet.mincost[0] = 7;
	packet.mincost[1] = INFINITY;
	packet.mincost[2] = 2;
	packet.mincost[3] = 0;
	
	printf("inside rtinit3\n");
	printf("rtinit3: %f\n", clocktime);
	int i;
	int j;

	for(i=0;i<4;i++)
	{
		for(j=0; j<4; j++)
		{
			dt3.costs[i][j]= 999;

		}
	}

    	dt3.costs[0][0]=7;
	dt3.costs[2][2]=2;
	dt3.costs[3][3]=999;

	for(i=0;i<4;i++)
	{
		for(j=0; j<4; j++)
		{
			if(packet.mincost[i] > dt3.costs[i][j])
				packet.mincost[i] = dt3.costs[i][j];
		}
	}

	packet.sourceid = 3;
	packet.destid = 0;
	tolayer2(packet);

	packet.sourceid = 3;
	packet.destid = 2;
	tolayer2(packet);

	packet.sourceid = 3;
	//printdt3(&dt3);
	printf("done with rtinit3\n");
}

/*This function updates the distance vector table after changes have been made to it */
void rtupdate3(rcvdpkt)
  struct rtpkt *rcvdpkt;
{
	packet.mincost[0] = 999;
	packet.mincost[1] = 999;
	packet.mincost[2] = 999;
	packet.mincost[3] = 999;

	printf("inside rtupdate3\n");

	extern float clocktime;
	printf("rtupdate3: %f\n sender: %d\n", clocktime, rcvdpkt->sourceid);
	//printdt3(&dt3);


	int change = 0;
	int i;
	int j;
	for(i = 0; i < 4; ++i)
	{
		if(dt3.costs[i][rcvdpkt->sourceid] > rcvdpkt->mincost[i] + dt3.costs[rcvdpkt->sourceid][rcvdpkt->sourceid])
		{
			dt3.costs[i][rcvdpkt->sourceid] = rcvdpkt->mincost[i] + dt3.costs[rcvdpkt->sourceid][rcvdpkt->sourceid];
			change = 1;
		}
	}
	if(change)
	{
		for(i=0;i<4;i++)
		{
			for(j=0; j<4; j++)
			{
				if(packet.mincost[i] > dt3.costs[i][j]) 
					packet.mincost[i] = dt3.costs[i][j];
			}
		}

		printf("finished updating distance table\n");


		//sends out data to data link layer after table has been updated 

		packet.sourceid = 3;
		packet.destid = 0;
		tolayer2(packet);

		packet.sourceid = 3;
		packet.destid = 2;
		tolayer2(packet);
	}
	//pretty prints the table after all changes are made
	//printdt3(&dt3);
}



