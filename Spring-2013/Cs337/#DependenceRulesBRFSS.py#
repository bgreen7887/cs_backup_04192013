1
"""
This file is adapted from:

This file contains code for use with "Think Stats",
by Allen B. Downey, available from greenteapress.com

Copyright 2010 Allen B. Downey
License: GNU GPLv3 http://www.gnu.org/licenses/gpl.html
"""

import math
import sys
import survey



def Mean(t):
    """Computes the mean of a sequence of numbers.

    Args:
        t: sequence of numbers

    Returns:
        float
    """
    return float(sum(t)) / len(t)

def Var(t, mu=None):
    """Computes the variance of a sequence of numbers.

    Args:
        t: sequence of numbers
        mu: value around which to compute the variance; by default,
            computes the mean.

    Returns:
        float
    """
    if mu is None:
        mu = Mean(t)

    # compute the squared deviations and return their mean.
    dev2 = [(x - mu)**2 for x in t]
    var = Mean(dev2)
    return var

def Cov(s, t):
    """Computes the covariance of two sequences of numbers.

    Args:
        s, t: sequences of numbers

    Returns:
        float
    """
    mu_s = Mean(s)
    mu_t = Mean(t)

    # compute the squared deviations and return their mean.
    dev2 = 0.0
    for i in range(len(s)):
        dev2 += (s[i] - mu_s)*(t[i]-mu_t)
    covar = dev2/len(s)
    return covar

def Cor(s, t):
    """Computes the correlation between two sequences of numbers.

    Args:
        s, t: sequences of numbers

    Returns:
        float
    """

    cor = Cov(s,t)/math.sqrt(Var(s)*Var(t))
    return cor



def MeanVar(t):
    """Computes the mean and variance of a sequence of numbers.

    Args:
        t: sequence of numbers

    Returns:
        tuple of two floats
    """
    mu = Mean(t)
    var = Var(t, mu)
    return mu, var



class Respondents(survey.Table):
    """Represents the respondent table."""

    def ReadRecords(self, data_dir='.', n=None):
        filename = self.GetFilename()
        self.ReadFile(data_dir,
                      filename,
                      self.GetFields(), 
                      survey.Respondent,
                      n)
        self.Recode()

    def GetFilename(self):
        """Get the name of the data file.

        This function can be overridden by child classes.

        The BRFSS data is available from thinkstats.com/CDBRFS08.ASC.gz
        return 'CDBRFS08.ASC.gz'

        """
        return 'LLCP2011.ASC'

    def GetFields(self):
        """Returns a tuple specifying the fields to extract.
        
        BRFSS codebook 
        http://www.cdc.gov/brfss/technical_infodata/surveydata/2008.htm

        The elements of the tuple are field, start, end, case.

                field is the name of the variable
                start and end are the indices as specified in the NSFG docs
                case is a callable that converts the result to int, float, etc.
        """
        return [
            ('age', 108, 109, int),
            ('weight', 126, 129, int),
            ('height', 130, 133, int),
            ('sex', 151, 151, int),
            ('genHealth', 73, 73, int),
            ('cholesterol', 88, 88, int),
            ('heartAttack', 89, 89, int),
            ('heartDesease', 90, 90, int),
            ('stroke', 91, 91, int),
            ('nonSkinCancer', 95, 95, int),
            ('diabetes', 101, 101, int),
            ('smoke', 103, 103, int),
            ]

    def Recode(self):
        """Recode variables that need cleaning."""

        for rec in self.records:

            # recode weight
            if rec.weight in [7777, 9999]:
                rec.weight = 'NA'
            elif 9000 < rec.weight < 9999:
                rec.weight = (rec.weight - 9000)*2.2

            # recode height
            if 199 < rec.height < 801:
              rec.height = rec.height%100 + 12*((rec.height-
                                                  rec.height%100)/100)
            elif 9000 < rec.height < 9300: # convert metric to English
              rec.height = (rec.height-9000)/2.54
            else:
                rec.height = 'NA'

            # recode age
            if rec.age in [7, 9]:
                rec.age = 'NA'

            # recode genHealth
            if rec.genHealth in [7, 9]:
                rec.genHealth = 'NA'

            # recode nonSkinCancer
            if rec.nonSkinCancer in [7, 9]:
                rec.nonSkinCancer = 'NA'

            # recode cholesterol
            if rec.cholesterol in [7, 9]:
                rec.cholesterol = 'NA'

            # recode heartAttack
            if rec.heartAttack in [7, 9]:
                rec.heartAttack = 'NA'

            # recode heartDesease
            if rec.heartDesease in [7, 9]:
                rec.heartDesease = 'NA'

            # recode stroke
            if rec.stroke in [7, 9]:
                rec.stroke = 'NA'

            # recode diabetes
            if rec.diabetes in [7, 9]:
                rec.diabetes = 'NA'

            # recode smoke
            if rec.smoke in [7, 9]:
                rec.smoke = 'NA'



    def BinaryVariables(self):
        """ compute binary factors:
          factor     incicates
            0         age<40
            1         male
            2         good health (genHealth >= 3)
            3         body mass index <= 25
            4         nonsmoker         
            5         no cancer
            6         no diabetes
            7         no stroke
            8         healthy heart (no heart attack or heart desease)
                                """
        numFacts = 9
        binTable = [ [ 0 for i in range(numFacts)]
                    for j in range(len(self.records))]
        count = -1
        for rec in self.records:
            count = count + 1

            if rec.age == 'NA':
                binTable[count][0] = 'NA'
            elif rec.age < 40:
                binTable[count][0] = 1

            if rec.sex == 1:  # male
                binTable[count][1] = 1

            if rec.genHealth == 'NA':
                binTable[count][2] = 'NA'
            elif rec.genHealth > 2:
                binTable[count][2] = 1

            if rec.weight == 'NA' or rec.height == 'NA':
                binTable[count][3] = 'NA'
            else:
                bmi = rec.weight * 703 /(rec.height*rec.height)
                if bmi < 26:
                    binTable[count][3] = 1

            if rec.smoke == 'NA':  
                binTable[count][4] = 'NA'
            elif rec.smoke == 3:  #nonsmoker
                binTable[count][4] = 1

            if rec.nonSkinCancer == 'NA':  
                binTable[count][5] = 'NA'
            elif rec.nonSkinCancer == 2:  # no nonSkinCancer
                binTable[count][5] = 1

            if rec.diabetes == 'NA':  
                binTable[count][6] = 'NA'
            elif rec.diabetes == 3:  # no diabetes
                binTable[count][6] = 1

            if rec.stroke == 'NA':  
                binTable[count][7] = 'NA'
            elif rec.stroke == 2:  # no stroke
                binTable[count][7] = 1

            if rec.heartAttack == 'NA' or rec.heartDesease == 'NA':  
                binTable[count][8] = 'NA'
            elif rec.heartAttack == 2 and rec.heartDesease == 2:  # no problems
                results = [0] * 9

            binTable[count][8] = 1
            for i in range(count):
        for j in range(9):
            if binTable[i][j] == 1:
                results[j] += 1
    print "COUNT: %s\n" %(count)
                        
    for i in range(9):
        print "Binary Sum %s: %s" % (i, results[i])
        print "Binary Factor %s: %s" % (i, float(results[i]/float(count)))
                                        
def main(name, data_dir='.'):
    resp = Respondents()
    resp.ReadRecords(data_dir)
    resp.BinaryVariables()
    
    
if __name__ == '__main__':
    main(*sys.argv)
