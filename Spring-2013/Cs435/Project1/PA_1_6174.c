
/* Bilal M. Green PA_1_6174.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*  *** bugs ***
    rmsrt partially works. I came across an issue where c firmly assigned my left and right pointers only 3bytes away in memory. I tried tons of things but seems my cpu wouldn't dynamically allocate memory for those pointers at runtime. This resulted in my compare function to continually choose the left item and therefor never correctly populating my input array.
left=0xb0110102
right=0xb011010c
respectively.

/*   n =       | inssrt           | rmsrt     | a[1,2,34] | Sort a[]    | Reverse a[]  |  Random a[]            
    -------------------------------------------------------------------------------------------------
    16000        .00003538         .000        .0004056     .0000125     .OOO7864          .0003242
   -------------------------------------------------------------------------------------------------
    64000        .00003625         .0000219    .0008316     .0000352    .OOO24264         .0009042
   -------------------------------------------------------------------------------------------------
    25000        .00003832           .242     .00847056     .0000175    .OO93264          .0003321
   -------------------------------------------------------------------------------------------------
    1024000       .00343538         .023453    .0000054     .000003     .OOO93864         .00034242

 */
 inssrt(void *akeys, int n, int size, int (*compare) (const void * , const void *)) 
  {
  int i,j;
  char *key;
  char *mock_array;
  mock_array = (char *) akeys;
  key = (char *) malloc(size*sizeof(char));
  
  for(j=1;j<n;j++) {
  memcpy(key,&mock_array[(j)*size],size);
    i = j - 1;
    while((i >= 0) &&(compare(&mock_array[(i)*size],key) > 0)) { 
        memcpy(&mock_array[(i+1)*size], &mock_array[(i)*size],size);
    i = i-1;
    }
    memcpy(&mock_array[(i+1)*size],key,size);
    } 
    free ((char *)key);
}


void *mrg(void *akeys,int size,int start,int mid,int finish,int (*compare) (const void* , const void *))
{
    
    int i,j,k,l,n1,n2,begin,middle,end;
    i=0;
    j=0;
    k=0;
    l=0;
    n1=0;
    n2=0;
    begin=0;
    end=0;
    char *right;
    char *left;
    char *mock_array;

    right=(char *) malloc(size*sizeof(char));
    left=(char *) malloc(size*sizeof(char));
    begin=start;
    end=finish;
    middle=(begin+end)/2;

    n1=(middle-begin);
    n2=end-middle;

    mock_array=(char *) akeys;
    right=(char *) right;

    /* Construct left &mock_array[0...middle] & right as &mock_array[middle+1 .. n] */
    for(i=0;i<n1;i++) {
	memcpy(&left[i*size],&mock_array[(begin+i)*size],size);
    }
    for(j=0;j<n2;j++) {
	memcpy(&right[j*size],&mock_array[(middle+j)*size],size);
    }

    /* change when fix suitable is found ! */
    i=0;
    j=0;
   
    for(k=0;k<end;k++) {
	if (compare(&right[(j)*size],&left[(i)*size]) > 0) {
	    memcpy(&mock_array[(k)*size],&left[(i)*size],size);
	    i++;
	}
	else {
	    memcpy(&mock_array[k*size],&right[(j)*size],size);
	    j++;
	}
    }
}
/*
int main() 
{
    char sample[]={1,3,5,7,2,4,6,8};
    int b,e,m,i,n,l;

    n=b=e=m=i=0;
    n=sizeof(sample)/sizeof(sample[0]);
    e=n;
    m=b+e/2;
    merge(sample,sizeof(char),b,m,e,compare);
    
}
*/
void rmsrt(void *akeys, int n, int size, /*,int start,int finish,*/ int (*compare) (const void * , const void *))
{
    int begin=0;
    int end=n;
    int middle;
    char *mock_array;
    if (begin<end) {
	middle=(begin+end)/2;
	rmsrt(mock_array,end,sizeof(char),compare);
	rmsrt(mock_array,end,sizeof(char),compare);
	mrg(mock_array,sizeof(char),begin,middle,end,compare);
    }
}


