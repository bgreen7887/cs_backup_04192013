/* Bilal M. Green PA_2_6174.java */

/*   n =       | arr[1234.5 .]   | arr[0 ..n-1] |arr[n-1 ..0] | Random            
    ------------------------------------------------------------------
    64000        .00003538          .000         .0004056     .0000125  
   -------------------------------------------------------------------
    512000       .00003625          .0000219     .0008316     .0000352  
   -------------------------------------------------------------------
    10240000     .00003832          .242         .00847056    .0000175  
   -------------------------------------------------------------------
    81924000       .00343538         .023453     .0000054     .000003   

 */
import java.io.*;
import java.util.Stack;
import java.util.*;
import java.util.Random;

public class PA_2_6174 {
    public static void main(String[] args) {
	
	//double [] myarr={29.0,345.0,3.0,23.0,94.0,10000.0,3.0,2.0,2223.0,82323.0,7.0,1.0,3.0,5.0,6.0};
	int n = 9;
	int few=6;
	double[] myarr = new double[n];
	Random ran = new Random();
	
	/* just change n for problem size */
	System.out.println("\t\t\t\t BEFORE NCLRSSRT ");
	System.out.print("[" );
	for ( int i=0; i < n; i++) {
	    myarr[i] = ran.nextDouble();
	}

	for(int i=0; i<= myarr.length -1; i++) {
	    System.out.print( myarr[i] + " ");   
	}
	
	/*
	for(int i=myarr.length -1; i>=0; i--) {
	    System.out.print( myarr[i] + " " );   
	}
	*/
	System.out.println("]" );
	System.out.println(" " );
	long start = System.currentTimeMillis();
		
	nclrssrt(myarr,myarr.length);
	
	long stop = System.currentTimeMillis();
	long elapsedTime = stop - start;
	System.out.println("Time elapsed " + elapsedTime);

	System.out.println("\t\t\t\t AFTER NCLRSSRT ");	
	System.out.print("[");
	/*
	for(int i=0; i<= few; i++) {
	    System.out.print( myarr[i] + " ");   
	}
	System.out.print( " ..... " );
	*/
	//	System.out.print(" " );
	for(int i=0; i<=myarr.length -1; i++) {
	    System.out.print( myarr[i] + " ");   
	}
	System.out.println("]" );
    }
	

    public static void nclrssrt(double arr_qui[], int n) {
	double l = 0;
	double r = arr_qui.length-1;
	Stack<Double> stack= new <Double>Stack();
	stack.push(l);
	stack.push(r);


	/* replaced recursive calls replaced w/ pushes and pops to the stack while !empty */
	while( !stack.empty()) {
	    r=stack.pop();
	    l=stack.pop();
	    if( r <= l) continue;
	    double i = partition(arr_qui,(int)l,(int)r);
	    
	    if (i-l > r-i) {
		stack.push(l);
		stack.push(i-1);
	    }
	    stack.push(i+l);
	    stack.push(r);
	  
	    if ( r-i >= i-l) {
		stack.push(l);
		stack.push(i-l);
	    }
	}
    }
    
    public static double partition(double arr[], int left, int right) {
	int i;
	int j;
	double tmp;
	double x=arr[right];
	i=left -1;
	
	for(j=left; j<=right -1.0;j++) {
	    if (arr[j] <= x) {
		i=i+1;
		tmp=arr[i];
		arr[i]=arr[j];
		arr[j]=tmp;	
	    }
	}
	tmp=arr[i+1];
	arr[i+1]=x;
	 arr[right]= tmp;
	return i+1;
    }
    
}

	/*
	System.out.print("[");   
	for(int i=0;i<=myarr.length-1;i++) {
	    System.out.print( myarr[i] + " " );   
	}
	System.out.println("]");   

	partition(myarr,l,r);

	System.out.print("[");

	for(int i=0;i<=myarr.length-1;i++) {
	    System.out.print(myarr[i] + " " );   
	    
	}
	System.out.println("]");
	
	
	
	int j = partition(myarr,l,r);
	System.out.println(j);
	*/

/*
  check swap
	double x,y,tmp;
        x = myarr[0];
	y = myarr[1];
	System.out.println("x = " + x + "y = " + y );
	
	tmp = x;
	x=y;
	y=tmp;
	
	System.out.println("x = " + x + "y = " + y );
	*/
	
	/* print original array call nclrssrt() and print array after function call */
