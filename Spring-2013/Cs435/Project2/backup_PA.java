import java.io.*;
import java.util.Stack;
import java.util.*;
public class PA_2_6174 {
    public static void main(String[] args) {
	
	int [] myarr={29,345,3,23,94,10000,3,2,2223,82323,7,1,3,5,6};
	
	/*
	  check swap
	int x,y,tmp;
        x = myarr[0];
	y = myarr[1];
	System.out.println("x = " + x + "y = " + y );
	
	tmp = x;
	x=y;
	y=tmp;
	
	System.out.println("x = " + x + "y = " + y );
	*/
	
	/* print original array call nclrssrt() and print array after function call */
	System.out.print("[");   
	for(int i=0;i<=myarr.length-1;i++) {
	    System.out.print( myarr[i] + " " );   
	}
	System.out.println("]");   
	
	nclrssrt(myarr,myarr.length);

	System.out.print("[");   
	for(int i=0;i<=myarr.length-1;i++) {
	    System.out.print( myarr[i] + " " );   
	}
	System.out.println("]");   
	
    }

    public static void nclrssrt(int arr_qui[], int n) {
	int l = 0;
	int r = arr_qui.length-1;
	Stack<Integer> stack= new <Integer>Stack();
	stack.push(l);
	stack.push(r);


	/* replaced recursive calls replaced w/ pushes and pops to the stack while !empty */
	while( !stack.empty()) {
	    r=stack.pop();
	    l=stack.pop();
	    if( r <= l) continue;
	    int i = partition(arr_qui,l,r);
	    
	    if (i-l > r-i) {
		stack.push(l);
		stack.push(i-1);
	    }
	    stack.push(i+l);
	    stack.push(r);
	  
	    if ( r-i >= i-l) {
		stack.push(l);
		stack.push(i-l);
	    }
	}
    }
    
    public static int partition(int arr[], int left, int right) {
	int x,i,j,tmp;
	x=arr[right];
	i=left-1;
	
	for(j=left; j<=right -1;j++) {
	    if (arr[j] <= x) {
		i=i+1;
		tmp=arr[i];
		arr[i]=arr[j];
		arr[j]=tmp;	
	    }
	}
	tmp=arr[i+1];
	arr[i+1]=x;
	 arr[right]= tmp;
	return i+1;
    }
    
}

	/*
	System.out.print("[");   
	for(int i=0;i<=myarr.length-1;i++) {
	    System.out.print( myarr[i] + " " );   
	}
	System.out.println("]");   

	partition(myarr,l,r);

	System.out.print("[");

	for(int i=0;i<=myarr.length-1;i++) {
	    System.out.print(myarr[i] + " " );   
	    
	}
	System.out.println("]");
	
	
	
	int j = partition(myarr,l,r);
	System.out.println(j);
	*/
