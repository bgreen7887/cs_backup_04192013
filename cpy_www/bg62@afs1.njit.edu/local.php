<?php
include('Crypt/AES.php');
$cipher = new Crypt_AES(CRYPT_AES_MODE_ECB);
$cipher->setKey('abcdefghijklmnopijklmnop');

$data = file_get_contents("php://input");
$encrypted_data = $cipher->encrypt($data);

$ch = curl_init('web.njit.edu/~mb252/index.php');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $encrypted_data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Content-Type: application/json',
	'Content-Length: ' . strlen($encrypted_data))
); 

$response = trim(curl_exec($ch));
echo $cipher->decrypt($response);
?>